var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var knex = require("../../knex");
var middleware = require("../middleware.js");
var constants = require("../../utilities/constants");
var mapper = require("../../utilities/mapper");

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get("/", function(req, res) {
  try {
    knex("sports")
      .select("*")
      .then(rows => {
        var data = rows.map( x=> mapper.map(x, mapper.SportsDBToUserModel));
        res.status(200).json({ responseCode: 20000, message: constants.getResponseMessage(20000), data: data });
      })
      .catch(err => {
        res.status(400).json({ responseCode: 40000, message: constants.getResponseMessage(40000) });
      });
  } catch (err) {
    res.status(400).json({ responseCode: 40000, message: constants.getResponseMessage(40000) });
  }
});

router.get("/categories", function(req, res) {
  try {
    knex("sport_category")
      .select("*")
      .then(rows => {
        var data = rows.map( x=> mapper.map(x, mapper.SportCategoriesDBToUserModel));
        res.status(200).json({ responseCode: 20000, message: constants.getResponseMessage(20000), data: data });
      })
      .catch(err => {
        res.status(400).json({ responseCode: 40000, message: constants.getResponseMessage(40000) });
      });
  } catch (err) {
    res.status(400).json({ responseCode: 40000, message: constants.getResponseMessage(40000) });
  }
});

module.exports = router;
