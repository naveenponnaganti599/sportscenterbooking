var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var knex = require("../../knex");
var checksum_lib = require("../../utilities/paytm/checksum");
var randomstring = require("randomstring");
var paytmConfig = require("../../utilities/paytm/paytm_config").paytm_config;
const Paytm = require("paytm-pg-node-sdk"); 


var environment = Paytm.LibraryConstants.PRODUCTION_ENVIRONMENT;

// For Production
// var environment = Paytm.LibraryConstants.PRODUCTION_ENVIRONMENT;

// Find your mid, key, website in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
var mid = paytmConfig.MID;
var key = paytmConfig.MERCHANT_KEY;
var website = paytmConfig.WEBSITE;
Paytm.MerchantProperties.initialize(environment, mid, key, website);
var channelId = Paytm.EChannelId.APP;

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.post("/getpaymentstatus", function (req, res) {
  try {
    var orderId = req.body.orderId;
    var readTimeout = 80000;
    var paymentStatusDetailBuilder = new Paytm.PaymentStatusDetailBuilder(
      orderId
    );
    var paymentStatusDetail = paymentStatusDetailBuilder
      .setReadTimeout(readTimeout)
      .build();
    Paytm.Payment.getPaymentStatus(paymentStatusDetail)
      .then((result) => {
        if(JSON.parse(result.jsonResponse).body.resultInfo.resultStatus == 'TXN_SUCCESS'){
          var result = JSON.parse(result.jsonResponse).body;
          console.log(result);
          knex.raw("update eventOrders set success = (?), transaction_id = (?) where payment_order_id = ?",[1, result.txnId, req.body.orderId ])
          .then((data)=>{
            res.status(200).json(result);
          })
          .catch(err => {
            console.log(err);
            res
            .status(400)
            .json({ message: "Error in updating event order" });
          });
        }
        else {
          res.status(200).json(JSON.parse(result.jsonResponse).body);
        }
      })
      .catch((err) => {
        res
          .status(400)
          .json({ message: "Error in fetching the status of payment" });
      });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});



router.post("/getChecksum", function (req, res) {
  try {
    var paytmParams = {};

    var orderId = randomstring.generate({
      length: 12,
      charset: "alphabetic",
    });

    /* put checksum parameters in Object */
    paytmParams["MID"] = paytmConfig.MID;
    paytmParams["ORDERID"] = orderId;
    paytmParams["CHANNELID"] = paytmConfig.CHANNEL_ID;
    Paytm.MerchantProperties.setCallbackUrl('https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=' + orderId);

    checksum_lib.genchecksum(paytmParams, paytmConfig.MERCHANT_KEY, function (
      err,
      checksum
    ) {
      if (!err) {
        // var channelId = Paytm.EChannelId.WEB;
        var txnAmount = Paytm.Money.constructWithCurrencyAndValue(
          Paytm.EnumCurrency.INR,
          req.body.amount
        );
        var userInfo = new Paytm.UserInfo("order" + orderId);
        // userInfo.setAddress("CUSTOMER_ADDRESS");
        // userInfo.setEmail("CUSTOMER_EMAIL_ID");
        // userInfo.setFirstName("CUSTOMER_FIRST_NAME");
        // userInfo.setLastName("CUSTOMER_LAST_NAME");
        // userInfo.setMobile("CUSTOMER_MOBILE_NO");
        // userInfo.setPincode("CUSTOMER_PINCODE");
        var paymentDetailBuilder = new Paytm.PaymentDetailBuilder(
          channelId,
          orderId,
          txnAmount,
          userInfo
        );
        var paymentDetail = paymentDetailBuilder.build();
        Paytm.Payment.createTxnToken(paymentDetail)
          .then((result) => {
            res.status(200).json({
              message: "transaction token fetched successfully",
              orderId: orderId,
              checksum: checksum,
              result: result.responseObject.body,
            });
          })
          .catch((err) => {
            res
              .status(500)
              .json({ message: "Error in generating transaction token." });
          });
      } else {
        res.status(500).json({ message: "Error in generating checksum." });
      }
    });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});

router.post("/validateChecksum", function (req, res) {
  try {
    var paytmParams = {};
    console.log(req.body.orderId);
    /* put checksum parameters in Object */
    paytmParams["MID"] = paytmConfig.MID;
    paytmParams["ORDERID"] = req.body.orderId;

    var isValid = checksum_lib.verifychecksum(
      paytmParams,
      paytmConfig.MERCHANT_KEY,
      req.body.checksum
    );
    if (isValid) {
      res.status(200).json({ message: "Checksum Matched" });
    } else {
      res.status(200).json({ message: "Checksum Mismatched" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});

router.post("/initiaterefund", function (req, res) {
  try {
    var orderId = req.body.orderId;

    var refId =
      "ref" +
      randomstring.generate({
        length: 9,
        charset: "alphabetic",
      });
    var txnId = req.body.txnId;
    var txnType = "REFUND";
    var refundAmount = req.body.amount;
    var readTimeout = 80000;
    var refund = new Paytm.RefundDetailBuilder(
      orderId,
      refId,
      txnId,
      txnType,
      refundAmount
    );
    var refundDetail = refund.setReadTimeout(readTimeout).build();
    Paytm.Refund.initiateRefund(refundDetail)
      .then((result) => {
        res.status(200).json(result.responseObject.body);
      })
      .catch((err) => {
        res
          .status(200)
          .json({ message: "Error in iniating the refund of payment" });
      });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});

router.post("/getrefundstatus", function (req, res) {
  try {
    var orderId = req.body.orderId;
    var refId = req.body.refId;
    var readTimeout = 8000;
    var refundStatusDetailBuilder = new Paytm.RefundStatusDetailBuilder(
      orderId,
      refId
    );
    var refundStatusDetail = refundStatusDetailBuilder
      .setReadTimeout(readTimeout)
      .build();
    Paytm.Refund.getRefundStatus(refundStatusDetail)
      .then((result) => {
        res.status(200).json(result.responseObject.body);
      })
      .catch((err) => {
        res
          .status(200)
          .json({ message: "Error in fetching the refund status of payment" });
      });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});


module.exports = router;
