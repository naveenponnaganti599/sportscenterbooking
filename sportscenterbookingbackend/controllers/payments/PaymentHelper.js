var paytmConfig = require("../../utilities/paytm/paytm_config").paytm_config;
const Paytm = require("paytm-pg-node-sdk");
var randomstring = require("randomstring");
var knex = require('../../knex.js');

var environment = Paytm.LibraryConstants.PRODUCTION_ENVIRONMENT;

// For Production
// var environment = Paytm.LibraryConstants.PRODUCTION_ENVIRONMENT;

// Find your mid, key, website in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
var mid = paytmConfig.MID;
var key = paytmConfig.MERCHANT_KEY;
var website = paytmConfig.WEBSITE;
Paytm.MerchantProperties.initialize(environment, mid, key, website);
var channelId = Paytm.EChannelId.APP;

var getChecksum = function (amount, orderId) {
  try {
    var amountString = amount.toString();
    var paytmParams = {};

    /* put checksum parameters in Object */
    paytmParams["MID"] = paytmConfig.MID;
    paytmParams["ORDERID"] = orderId;
    paytmParams["CHANNELID"] = paytmConfig.CHANNEL_ID;

    Paytm.MerchantProperties.setCallbackUrl('https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=' + orderId);

    var txnAmount = Paytm.Money.constructWithCurrencyAndValue(
      Paytm.EnumCurrency.INR,
      amountString
    );
    var userInfo = new Paytm.UserInfo("order" + orderId);
    // userInfo.setAddress("CUSTOMER_ADDRESS");
    // userInfo.setEmail("CUSTOMER_EMAIL_ID");
    // userInfo.setFirstName("CUSTOMER_FIRST_NAME");
    // userInfo.setLastName("CUSTOMER_LAST_NAME");
    // userInfo.setMobile("CUSTOMER_MOBILE_NO");
    // userInfo.setPincode("CUSTOMER_PINCODE");
    var paymentDetailBuilder = new Paytm.PaymentDetailBuilder(
      channelId,
      orderId,
      txnAmount,
      userInfo
    );
    var paymentDetail = paymentDetailBuilder.build();
    return Paytm.Payment.createTxnToken(paymentDetail);
  } catch (err) {
    throw err;
  }
};

var refund = function(amount, orderId) {
  try {
   return knex('eventorders').select("transaction_id")
    .where("payment_order_id", orderId)
    .then((result)=>{
      var refId =
      "ref" +
      randomstring.generate({
        length: 9,
        charset: "alphabetic",
      });
      var transactionId = result.pop().transaction_id;
      var txnType = "REFUND";
      var refundAmount = amount;
      var readTimeout = 80000;
      var refund = new Paytm.RefundDetailBuilder(
        orderId,
        refId,
        transactionId,
        txnType,
        refundAmount
      );
      var refundDetail = refund.setReadTimeout(readTimeout).build();
      return Paytm.Refund.initiateRefund(refundDetail);
    })
    .catch((err)=>{
      console.log(err);
      return new Promise(function(resolve, reject) { 
          reject("record not found in db"); 
        }) ;
    })
  } catch (err) {
    return new Promise(function(resolve, reject) { 
      reject("error in initiating refund"); 
    });
  }
}
module.exports = {
  getChecksum: getChecksum,
  refund: refund
};
