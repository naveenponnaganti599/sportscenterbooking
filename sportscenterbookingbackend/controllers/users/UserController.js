var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var config = require('../../config');
var knex = require('../../knex');
var axios = require('axios');
var middleware = require("../middleware.js");
var jwt = require('jsonwebtoken');
var mapper = require('../../utilities/mapper');
var messages = require('../../utilities/constants');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.post('/addUpdateUser', middleware, function (req, res) {
    try {
        var dbUserModel = mapper.map(req.body.user, mapper.UsersUserToDBModel);
        var userModel = null;
        var userId = -1;
        var roleId = -1;
        knex('users').select("*").where("mobile", req.body.user.mobile).then((rows) => {
            if(rows.length > 0) {
                dbUserModel.updated_at = new Date().toISOString();
                knex('users')
                .update(dbUserModel)
                .where("mobile", req.body.user.mobile)
                .then((rows)=>{
                    knex('users').select("users.*", "user_preferred_cities.city_id")
                    .leftJoin("user_preferred_cities", "users.id", "user_preferred_cities.user_id")
                    .where("mobile", req.body.user.mobile).then((rows) => {
                        if (rows && rows.length >= 1) {
                            userModel = mapper.map(rows[0], mapper.UsersDBToUserModel)
                            userId = userModel.userId;
                        }
                        knex('userrole').update({
                            role_id: req.body.user.role
                        }).where("user_id", userId)
                        .then((insertedId) => {
                            roleId = req.body.user.role;
                            userModel.role = roleId;
                            var token = generateToken(userId, req.body.user.mobile, roleId);
                            return res.status(200).json({ responseCode: 20005, message: messages.getResponseMessage(20005), data: userModel, token: token });
                        }).catch((err) => {
                            res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                        });
                    }).catch((err) => {
                        res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                    });
                }).catch((err) => {
                    res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                });

            } 
            else {
                dbUserModel.created_at = new Date().toISOString();
                knex('users').insert(dbUserModel).then((newId) => {
                    userId = newId.pop();
                    Promise.all([
                        knex('users').select("users.*", "user_preferred_cities.city_id")
                        .leftJoin("user_preferred_cities", "users.id", "user_preferred_cities.user_id")
                        .where("id", userId).then((rows) => {
                            if (rows && rows.length >= 1) {
                                userModel = mapper.map(rows[0], mapper.UsersDBToUserModel)
                                }
                        }).catch((err) => {
                            res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                        }), 
                        knex('userrole').insert({
                            user_id: userId,
                            role_id: req.body.user.role
                        }).then((insertedId) => {
                            roleId = req.body.user.role;
                            userModel.role = req.body.user.role;
                        }).catch((err) => {
                            res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                        })
                    ]).then(() => {
                        var token = generateToken(userId, req.body.user.mobile, roleId);
                        return res.status(200).json({ responseCode: 20001, message: messages.getResponseMessage(20001), data: userModel, token: token });
                    }).catch((err) => {
                        res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                    });
                }).catch((err) => {
                    res.status(400).json({ responseCode: 40001, message: messages.getResponseMessage(40001) });
                });
            }
        })
    }
    catch (err) {
        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
    }
});

router.post('/sendOTP', function (req, res) {
    try {
        const otp = Math.floor(1000 + Math.random() * 9000);
        knex('otps').select('id').where('mobile', req.body.phone).then((userOtp) => {
            axios.get(config.smsGatewayurl + "&mobiles=" + req.body.phone + "&message=Your OTP for SWYNG is " + otp.toString())
            .then((response) => {
                if(userOtp.length > 0) {
                    knex('otps').where('mobile', req.body.phone)
                    .update({
                        "otp": otp,
                        "otp_generation_time": new Date().toDateString()
                    })
                    .then((updatedData) => {
                        return res.status(200).json({ responseCode: 20002, message: messages.getResponseMessage(20002) });
                    }).catch(err => {
                        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
                    })
                } 
                else {
                    knex('otps').where('mobile', req.body.phone)
                    .insert({
                        "mobile": req.body.phone,
                        "otp": otp,
                        "otp_generation_time": new Date().toDateString()
                    })
                    .then((updatedData) => {
                        return res.status(200).json({ responseCode: 20002, message: messages.getResponseMessage(20002) });
                    }).catch(err => {
                        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
                    })
                }
            }).catch(err => {
                return res.status(400).json({ responseCode: 40002, message: messages.getResponseMessage(40002) });
            });
        }).catch(err => {
            return res.status(400).json({ responseCode: 40002, message: messages.getResponseMessage(40002) });
        });
    }
    catch (err) {
        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
    }
});

router.post('/validateOTP', function (req, res) {
    try {
        knex('otps').select("*").where('mobile', req.body.phone).then((rows) => {
            var userOtp = rows.pop();
            var userId = -1;
            var roleId = -1;
            if (userOtp && parseInt(userOtp.otp) == parseInt(req.body.otp)) {
                knex('users as u').select(['ur.role_id', 'u.id'])
                .join('userrole as ur','ur.User_id', 'u.id')
                .where('mobile', req.body.phone)
                .then((users) => {
                    if(users.length > 0) {
                        var user = users.pop();  
                        userId = user.id;
                        roleId = user.role_id;
                    }
                    var token = generateToken(userId, req.body.phone, roleId);
                    return res.status(200).json({ token, responseCode: 20003, message: messages.getResponseMessage(20003) });
                });
            } else {
                return res.status(400).json({ responseCode: 40004, message: messages.getResponseMessage(40004) });
            }
        }).catch(err => {
            return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
        });
    }
    catch (err) {
        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
    }
});

router.get('/getSignUpModel', middleware, function (req, res) {
    try {
        var data = {};
        Promise.all([
            knex('cities').select('*').then((rows) => {
                data.cities = rows.map(x=> mapper.map(x, mapper.CitiesDBToUserModel));
            }), knex('gender').select('*').then((rows) => {
                data.gender = rows.map(x=> mapper.map(x, mapper.GenderDBToUserModel));
            }), knex('tShirtSize').select('*').then((rows) => {
                data.tShirtSize = rows.map(x=> mapper.map(x, mapper.TShirtSizeDBToUserModel));
            }), knex('bloodGroup').select('*').then((rows) => {
                data.bloodGroup = rows.map(x=> mapper.map(x, mapper.BloodGroupDBToUserModel));
            })]).then((resultData) => {
                return res.status(200).json({ responseCode: 20000, message: messages.getResponseMessage(20000), data: data });
            }).catch((err) => {
                res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
            })
    }
    catch (err) {
        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
    }
});

router.get('/getUserDetails', middleware, function (req, res) {
    try {
        knex('users').select("users.*", "user_preferred_cities.city_id")
        .leftJoin("user_preferred_cities", "users.id", "user_preferred_cities.user_id")
        .where('mobile', req.phone).then((rows) => {
            var user = rows.pop();
            if (user) {
                var userModel = mapper.map(user, mapper.UsersDBToUserModel);
                return res.status(200).json({ responseCode: 20000, message: messages.getResponseMessage(20000), data: userModel });
            } else {
                return res.status(400).json({ responseCode: 40003, message: messages.getResponseMessage(40003) });
            }
        }).catch((err) => {
            return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
        });
    }
    catch (err) {
        return res.status(400).json({ responseCode: 50000, message: messages.getResponseMessage(50000) });
    }
});

router.get('/testToken', middleware, function(req, res) {
    return res.status(200).json("valid token"+ req.userId + "next" + req.roleId + "next" + req.phone);
});

router.post('/saveUserPreferredCity', middleware, function(req, res) {
    if(req.userId > 0) {
        knex('user_preferred_cities').select('city_id').where('user_id', req.userId).limit(1).then(rows=> {
            if(rows.length == 1) {
                var cityId = rows.pop().city_id;
                knex('user_preferred_cities').update({"city_id": req.body.cityId}).where("user_id", req.userId)
                .then((d) => {
                    return res.status(200).json({ responseCode: 20000, message: messages.getResponseMessage(20000) });
                })
                .catch((err) => {
                    return res.status(400).json({ responseCode: 40000, message: messages.getResponseMessage(40000) });
                })
            }
            else{
                knex('user_preferred_cities').insert({"city_id": req.body.cityId, "user_id": req.userId})
                .then((d) => {
                    return res.status(200).json({ responseCode: 20000, message: messages.getResponseMessage(20000) });
                })
                .catch((err) => {
                    return res.status(400).json({ responseCode: 40000, message: messages.getResponseMessage(40000) });
                })
            }
        });
    }
});

function generateToken(userId, phone, roleId) {
    const token = jwt.sign(
        {
            userId: userId,
            data: phone,
            role: roleId
        },
        config.secret,
        {
            expiresIn: '30d'
        }
    );
    return token;
}

module.exports = router;
