var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var knex = require("../../knex");
var middleware = require("../middleware.js");
var mapper = require("../../utilities/mapper");
var constants = require("../../utilities/constants");
var payments = require("../payments/PaymentController.js");
var paymentHelper = require("../payments/PaymentHelper.js");
var randomstring = require("randomstring");

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.post("/getEvents", middleware, function (req, res) {
  try {
    //we need to send events in particular city.
    var userId = req.body.userId;
    var cityId = req.body.cityId;
    var sportsId = req.body.sportIds;
    var subCategoryNames = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_names");

    var subCategoryIds = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.sport_category_id)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_ids");

    var registeredCount = knex("eventOrderItems as eoi")
      .select(knex.raw("sum(eoi.selected_slots)"))
      .innerJoin(
        "eventOrders as eo",
        "eo.event_orders_id",
        "eoi.event_order_id"
      )
      .whereRaw("eo.event_id = se.event_id")
      .as("registered_count");

    var interestedCount = knex("user_event_interests as uei")
      .select(knex.raw("count(*)"))
      .whereRaw("uei.event_id = se.event_id")
      .as("interested_count");

      var interested = knex("user_event_interests as uei")
      .select(knex.raw("count(*)"))
      .whereRaw("uei.event_id = se.event_id and uei.user_id = ?", userId)
      .as("interested");

    knex("sportevents as se")
      .leftJoin("sports as s", "s.sport_id", "se.sport_id")
      .select(
        "se.event_id",
        "se.event_name",
        "se.event_location",
        "se.event_start_date_time",
        "s.sport_id",
        "s.name as sport_Name",
        "se.submit_registered_user_details_by",
        "se.last_date_time_registration",
        knex.raw(
          "(select end_time from cancellationcharges where event_id = se.event_id and is_active = 1 order by end_time desc limit 1) as cancel_before"
        ),
        subCategoryNames,
        subCategoryIds,
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_primary = 1 limit 1) as file_path"
        ),
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_header = 1 limit 1) as header_file_path"
        ),
        interestedCount,
        registeredCount,
        knex.raw(
          "Exists(select 1 from user_event_interests uei where uei.event_id = se.event_id and uei.user_id = ? ) as interested",
          parseInt(userId)
        ),
        interested
      )
      .whereRaw(
        // "se.is_active = 1 and se.event_start_date_time > current_timestamp and se.event_city_id= ?",
         "se.is_active = 1 and se.event_city_id= ?",
        parseInt(cityId)
      )
      .where(function () {
        if (sportsId.length > 0)
          this.whereIn("se.sport_id", sportsId);
      })
      .then((rows) => {
        var result = rows.map((x) =>
          mapper.map(x, mapper.EventGalleyDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.post("/getParticipants", function (req, res) {
  knex("eventregistration as er")
    .innerJoin("eventOrders as eo", "eo.event_orders_id", "er.event_order_id")
    .select("*")
    .where("eo.event_id", req.body.eventId)
    .then((rows) => {
      if (rows && rows.length > 0) {
        var data = rows.map((x) =>
          mapper.map(x, mapper.EventOrderMemberDetailsDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: data,
        });
      } else
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
    })
    .catch((err) => {
      res.status(400).json({
        responseCode: 50000,
        message: constants.getResponseMessage(50000),
      });
    });
});

router.post("/runGallery", function (req, res) {
  try {
    //we need to send events in particular city.
    var userId = req.body.userId;
    var cityId = req.body.cityId;
    var subCategoryNames = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_names");

    var subCategoryIds = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.sport_category_id)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_ids");

    knex("sportevents as se")
      .leftJoin("sports as s", "s.sport_id", "se.sport_id")
      .leftJoin("eventImages as ei", "ei.event_id", "se.event_id")
      .leftJoin("eventOrders as eo", "eo.event_id", "se.event_id")
      .leftJoin(
        "eventOrderItems as eoi",
        "eoi.event_Order_id",
        "eo.event_orders_id"
      )
      .select(
        "se.event_name",
        "se.event_location",
        "se.event_start_date_time",
        "s.sport_id",
        "s.name as sport_Name",
        "ei.file_path",
        "se.submit_registered_user_details_by",
        "se.last_date_time_registration",
        knex.raw(
          "(select end_time from cancellationcharges where event_id = se.event_id and is_active = 1 order by end_time desc limit 1) as cancel_before"
        ),
        subCategoryNames,
        subCategoryIds,
        knex.raw("sum(eoi.selected_slots) as registered_count"),
        knex.raw(
          "Exists(select 1 from user_event_interests uei where uei.event_id = se.event_id and uei.user_id = ? ) as interested",
          parseInt(userId)
        )
      )
      .whereRaw(
        "se.is_active = 1 and ei.is_primary = 1 and se.is_run = 1 and se.event_start_date_time > current_timestamp and se.event_city_id= ?",
        parseInt(cityId)
      )
      .groupBy("eo.event_id", "se.event_id", "eoi.event_order_id")
      .then((rows) => {
        var result = rows.map((x) =>
          mapper.map(x, mapper.EventGalleyDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.get("/tournamentGallery", function (req, res) {
  try {
    var userId = req.body.userId;
    var cityId = req.body.cityId;
    var subCategoryNames = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_names");

    var subCategoryIds = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.sport_category_id)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_ids");

    knex("sportevents as se")
      .leftJoin("sports as s", "s.sport_id", "se.sport_id")
      .leftJoin("eventImages as ei", "ei.event_id", "se.event_id")
      .leftJoin("eventOrders as eo", "eo.event_id", "se.event_id")
      .leftJoin(
        "eventOrderItems as eoi",
        "eoi.event_Order_id",
        "eo.event_orders_id"
      )
      .select(
        "se.event_name",
        "se.event_location",
        "se.event_start_date_time",
        "s.sport_id",
        "s.name as sport_Name",
        "ei.file_path",
        "se.submit_registered_user_details_by",
        "se.last_date_time_registration",
        knex.raw(
          "(select end_time from cancellationcharges where event_id = se.event_id and is_active = 1 order by end_time desc limit 1) as cancel_before"
        ),
        subCategoryNames,
        subCategoryIds,
        knex.raw("sum(eoi.selected_slots) as registered_count"),
        knex.raw(
          "Exists(select 1 from user_event_interests uei where uei.event_id = se.event_id and uei.user_id = ? ) as interested",
          parseInt(userId)
        )
      )
      .whereRaw(
        "se.is_active = 1 and ei.is_primary = 1 and  se.event_start_date_time > current_timestamp and se.is_tournament = 1 and se.event_city_id= ?",
        parseInt(cityId)
      )
      .groupBy("eo.event_id", "se.event_id", "eoi.event_order_id")
      .then((rows) => {
        var result = rows.map((x) =>
          mapper.map(x, mapper.EventGalleyDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.post("/eventDetails", middleware, function (req, res) {
  try {
    //send event details screen information.

    var eventId = req.body.eventId;
    var result = {};
    var userId = req.userId;
    var subCategoryNames = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_names");

    var subCategoryIds = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.sport_category_id)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_ids");

    var registeredCount = knex("eventOrderItems as eoi")
      .select(knex.raw("sum(eoi.selected_slots)"))
      .innerJoin(
        "eventOrders as eo",
        "eo.event_orders_id",
        "eoi.event_order_id"
      )
      .whereRaw("eo.event_id = se.event_id")
      .as("registered_count");

    var interestedCount = knex("user_event_interests as uei")
      .select(knex.raw("count(*)"))
      .whereRaw("uei.event_id = se.event_id")
      .as("interested_count");

    var eventsPromise = knex("sportevents as se")
      .leftJoin("sports as s", "s.sport_id", "se.sport_id")
      .select(
        "se.event_id",
        "se.event_name",
        "se.event_location",
        "se.event_start_date_time",
        "s.sport_id",
        "s.name as sport_Name",
        "se.submit_registered_user_details_by",
        "se.last_date_time_registration",
        knex.raw(
          "(select end_time from cancellationcharges where event_id = se.event_id and is_active = 1 order by end_time desc limit 1) as cancel_before"
        ),
        subCategoryNames,
        subCategoryIds,
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_primary = 1 limit 1) as file_path"
        ),
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_header = 1 limit 1) as header_file_path"
        ),
        interestedCount,
        registeredCount,
        knex.raw(
          "Exists(select 1 from user_event_interests uei where uei.event_id = se.event_id and uei.user_id = ? ) as interested",
          parseInt(userId)
        )
      )
      .whereRaw(
        "se.is_active = 1 and se.event_start_date_time > current_timestamp and se.event_id= ?",
        parseInt(eventId)
      )
      .then((rows) => {
        var data = rows.pop();
        result = mapper.map(data, mapper.EventGalleyDBToUserModel);
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });

    var eventDetails = [];
    var eventDetailsPromise = knex("eventdetails")
      .select("header", "body", "display_order")
      .where("event_id", parseInt(eventId))
      .then((rows) => {
        if (rows && rows.length > 0) {
          eventDetails = rows.map((x) =>
            mapper.map(x, mapper.EventDetailsDBToUserModel)
          );
        }
      })
      .catch((err) => { });

    // var imageHeaderDetails = knex("sportevents as se")
    //   .innerJoin("eventImages as ei", "ei.event_id", "se.event_id")
    //   .select("se.event_name as eventName", "ei.file_path as filePath")
    //   .where("ei.is_header", 1)
    //   .andWhere("se.event_id", eventId)
    //   .then((rows) => {
    //     result.eventName = rows[0].eventName;
    //     result.imagePath = rows[0].filePath;
    //   })
    //   .catch((err) => {});

    Promise.all([eventDetailsPromise, eventsPromise])
      .then(() => {
        result.eventDetails = eventDetails;
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

//Insert into EventOrder
router.post("/saveEventOrder", async function (req, res) {
  try {
    //send event details screen information.
    var eventOrder = mapper.map(
      req.body.eventOrder,
      mapper.EventOrderUserToDBModel
    );
    var eventOrderItems = req.body.eventOrderItems;

    var orderId = randomstring.generate({
      length: 12,
      charset: "alphabetic",
    });

    await paymentHelper
      .getChecksum(eventOrder.amount_payable, orderId)
      .then((result) => {
        var returnData = {
          orderId: orderId,
          result: result.responseObject.body,
        };
        if (returnData && returnData.result.resultInfo.resultStatus == "S") {
          eventOrder.payment_order_id = returnData.orderId;
          eventOrder.success = false;
          knex("eventorders")
            .insert(eventOrder)
            .then((rows) => {
              var insertedOrderId = rows && rows.length > 0 ? rows.pop() : -1;
              if (insertedOrderId > 0) {
                var items = req.body.eventOrderItems.map((item) => {
                  item.event_order_id = insertedOrderId;
                  item.is_active = true;
                  return item;
                });
                knex("eventOrderItems")
                  .insert(items)
                  .then((rows) => {
                    knex("paymentDetails")
                      .insert([
                        {
                          payment_amount: eventOrder.amount_payable,
                          transaction_status:
                            returnData.result.resultInfo.resultMsg,
                          transaction_id: returnData.result.txnToken,
                          created_user_id: eventOrder.created_by_user_id,
                          created_at: eventOrder.created_at,
                          updated_at: "now()",
                          event_order_id: insertedOrderId,
                          is_refund_initiated: false,
                          reference_id: null,
                        },
                      ])
                      .then(() => {
                        res.status(200).json({
                          responseCode: 20000,
                          message: constants.getResponseMessage(20000),
                          data: {
                            eventOrderId: insertedOrderId,
                            paymentOrderId: eventOrder.payment_order_id,
                            txnToken: returnData.result.txnToken,
                            amount: eventOrder.amount_payable,
                          },
                        });
                      })
                      .catch((err) => {
                        res.status(400).json({
                          responseCode: 40000,
                          message: constants.getResponseMessage(40000),
                        });
                      });
                  })
                  .catch((err) => {
                    res.status(400).json({
                      responseCode: 40000,
                      message: constants.getResponseMessage(40000),
                    });
                  });
              }
            })
            .catch((err) => {
              res.status(400).json({
                responseCode: 40000,
                message: constants.getResponseMessage(40000),
              });
            });
        } else {
          res.status(400).json({
            responseCode: 40000,
            message: constants.getResponseMessage(40000),
          });
        }

        //return returnData;
      })
      .catch((err) => {
        throw err;
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 40000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/saveEventOrderMemberDetails", function (req, res) {
  try {
    //Insert data into table for intrested events.
    var data = mapper.map(
      req.body.eventOrderMemberDetails,
      mapper.EventOrderMemberDetailsUserToDBModel
    );
    knex("eventRegistration")
      .insert(data)
      .then((rows) => {
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 40000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/updateEventOrderMemberDetails", function (req, res) {
  try {
    var data = mapper.map(
      req.body.eventOrderMemberDetails,
      mapper.EventOrderMemberDetailsUserToDBModel
    );
    knex("eventRegistration")
      .update(data)
      .where("event_registration_id", parseInt(req.body.eventOrderMemberDetails.eventRegistrationId))
      .then((rows) => {
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 40000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/getEventOrderMemberDetails", function (req, res) {
  try {
    knex("eventRegistration as er")
      .join("eventCategories as ec", "ec.event_category_id", "er.event_category_id")
      .join("sport_category as sc", "ec.sport_category_id", "sc.sport_category_id")
      .select(["er.event_registration_id", "er.event_order_id", "er.first_name", "er.last_name",
        "er.phone_number", "er.email", "er.dob", "er.gender_id", "er.tshirt_size", "er.is_active",
        "er.is_self", "ec.sport_category_id", "ec.event_category_id", "ec.costs", "er.bloodGroupId",
        "er.emergency_contact_name", "er.emergency_contact_number"])
      .where("event_order_id", req.body.eventOrderId)
      .then((rows) => {
        if (rows && rows.length > 0) {
          var data = rows.map((x) =>
            mapper.map(x, mapper.EventOrderMemberDetailsDBToUserModel)
          );
          res.status(200).json({
            responseCode: 20000,
            message: constants.getResponseMessage(20000),
            data: data,
          });
        } else
          res.status(200).json({
            responseCode: 20000,
            message: constants.getResponseMessage(20000),
            data: []
          });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 40000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/pastrunregistrations", middleware, function (req, res) {
  try {
    var userId = parseInt(req.userId);
    knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_name, se.event_start_date_time, sc.sport_category_id, eoi.selected_slots, sc.name,not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time < current_timestamp and se.is_run = 1 and is_tournament = 0 and eo.success = 1 and eo.created_by_user_id = ?",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        );
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var runRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          runRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            cancelled: event.cancelled,
            sportCategories: sportCategories
          });
        });

        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: runRegistrations,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.post("/pasttournamentregistrations", middleware, function (req, res) {
  try {
    var userId = parseInt(req.userId);
    knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_name,se.event_venue, se.event_start_date_time, sc.sport_category_id, eoi.selected_slots, sc.name, not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time < current_timestamp and se.is_run = 0 and is_tournament = 1  and eo.success = 1 and eo.created_by_user_id = ?",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        );
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var tournamentRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          tournamentRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            cancelled: event.cancelled,
            sportCategories: sportCategories
          });
        });

        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: tournamentRegistrations,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});

router.post("/upcomingrunregistrations", middleware, function (req, res) {
  try {
    var userId = parseInt(req.userId);
    knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_name,se.event_venue, se.event_start_date_time, sc.sport_category_id, eoi.selected_slots, sc.name,not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time > current_timestamp and se.is_run = 1 and is_tournament = 0  and eo.success = 1 and eo.created_by_user_id = ? ",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) => {
          return mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        });
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var runRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          runRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            sportCategories: sportCategories,
            cancelled: event.cancelled
          });
        });

        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: runRegistrations,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.post("/upcomingtournamentregistrations", middleware, function (req, res) {
  try {
    var userId = parseInt(req.userId);
    knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_venue, se.event_name, se.event_start_date_time, sc.sport_category_id, eoi.selected_slots, sc.name, not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time > current_timestamp and se.is_run = 0 and is_tournament = 1  and eo.success = 1 and eo.created_by_user_id = ?",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        );
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var tournamentRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          tournamentRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            cancelled: event.cancelled,
            sportCategories: sportCategories
          });
        });

        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: tournamentRegistrations,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    return res.status(500).json({ message: "Error" });
  }
});

router.post("/loadDashboard", middleware, function (req, res) {
  try {
    var subcolumn = knex("banner_sport_categories as bsc")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "bsc.sport_category_id"
      )
      .whereRaw("b.banner_id = bsc.banner_id")
      .as("sport_category_names");

    var userId = req.userId;

    var bannersPromise = knex("banners as b")
      .innerJoin("sports as s", "s.sport_id", "b.sport_id")
      .innerJoin("banner_images as bi", "bi.banner_id", "b.banner_id")
      // .innerJoin("sportEvents as se", "b.event_id", "se.event_id")
      .select(
        "b.name as event_name",
        "b.address",
        "s.name as sport_name",
        "bi.image_path",
        "b.event_id",
        subcolumn
      )
      .where(function () {
        this.where("b.is_promotion_events", 1).andWhere(
          "b.city_id",
          parseInt(req.body.cityId)
        );
      })
      .orWhere(function () {
        if (req.body.sportsId.length > 0)
          this.whereRaw("b.last_date > current_timestamp")
            .andWhere("b.is_active", 1)
            .andWhere("b.city_id", parseInt(req.body.cityId))
            .whereIn("b.sport_id", req.body.sportsId);
        else
          this.whereRaw("b.last_date > current_timestamp")
            .andWhere("b.is_active", 1)
            .andWhere("b.city_id", parseInt(req.body.cityId))
      })
      .orderBy("b.is_promotion_events", "desc")
      .then((rows) => {
        result.banners = rows.map((x) =>
          mapper.map(x, mapper.BannerDBToUserModel)
        );
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });

    var runRegistrationsPromise = knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_name, se.event_start_date_time,se.event_venue, sc.sport_category_id, eoi.selected_slots, sc.name, not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time > current_timestamp and se.is_run = 1 and is_tournament = 0 and eo.success = 1 and eo.created_by_user_id = ?",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        );
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var runRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          runRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            cancelled: event.cancelled,
            sportCategories: sportCategories
          });
        });

        result.runRegistrations = runRegistrations;
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });

    var tournamentRegistrationsPromise = knex("sportevents as se")
      .innerJoin("eventorders as eo", "se.event_id", "eo.event_id")
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "eoi.sport_category_id"
      )
      .select(
        knex.raw(
          "eo.event_orders_id, se.event_id, se.event_name, se.event_start_date_time,se.event_venue, sc.sport_category_id, eoi.event_order_id, eoi.selected_slots, sc.name, not eo.is_active as cancelled"
        )
      )
      .whereRaw(
        "se.event_start_date_time > current_timestamp and se.is_run = 0 and is_tournament = 1  and eo.success = 1 and eo.created_by_user_id = ?",
        parseInt(userId)
      )
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.DashboardRegistrationsDBToUserModel)
        );
        const eventOrders = [...new Set(data.map(item => item.eventOrderId))];
        var tournamentRegistrations = [];
        eventOrders.forEach((element) => {
          var sportCategories = [];
          var eventOrderIdElement = data.filter(x => x.eventOrderId === element);
          eventOrderIdElement.forEach((eachElement) => {
            sportCategories.push({
              sportCategoryId: eachElement.sportCategoryId,
              sportCategoryName: eachElement.sportCategoryName,
              selectedSlots: eachElement.selectedSlots
            });
          });
          var event = data.find((x) => x.eventOrderId == element);
          tournamentRegistrations.push({
            eventOrderId: element,
            eventId: event.eventId,
            eventName: event.eventName,
            eventVenue: event.eventVenue,
            eventDate: event.eventStartDateTime,
            cancelled: event.cancelled,
            sportCategories: sportCategories
          });
        });

        result.tournamentRegistrations = tournamentRegistrations;
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });

    var supportContactInfoPromise = knex("adminConfiguration")
      .select(["email", "whatsapp", "contact_number"])
      .limit(1)
      .then((rows) => {
        var data = rows.pop();
        result.contactInfo = mapper.map(
          data,
          mapper.SupportContactInfoDBToUserModel
        );
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });

    var result = {};
    Promise.all([
      bannersPromise,
      runRegistrationsPromise,
      tournamentRegistrationsPromise,
      supportContactInfoPromise,
    ])
      .then(() => {
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(50000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.get("/getEventsFilter", middleware, function (req, res) {
  try {
    var result = {};

    var sportsPromise = knex("sports")
      .select("*")
      .then((rows) => {
        var data = rows.map((x) => mapper.map(x, mapper.SportsDBToUserModel));
        result.sports = data;
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });

    var sportCategoriesPromise = knex("sport_category")
      .select("*")
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.SportCategoriesDBToUserModel)
        );
        result.sportCategories = data;
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });

    var citiesPromise = knex("cities")
      .select("*")
      .then((rows) => {
        result.cities = rows.map((x) =>
          mapper.map(x, mapper.CitiesDBToUserModel)
        );
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 40000,
          message: constants.getResponseMessage(40000),
        });
      });

    Promise.all([sportsPromise, sportCategoriesPromise, citiesPromise])
      .then(() => {
        result.sports.forEach((sport) => {
          sport.categories = result.sportCategories.filter(
            (x) => x.sportId == sport.sportId
          );
        });
        var data = {
          sports: result.sports,
          cities: result.cities,
        };
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(50000),
    });
  }
});

router.post("/getEventCosts", function (req, res) {
  try {
    var subcolumn = knex("eventOrders as eo")
      .select(knex.raw("sum(eoi.selected_slots)"))
      .innerJoin(
        "eventOrderItems as eoi",
        "eoi.event_order_id",
        "eo.event_orders_id"
      )
      .whereRaw(
        "eo.event_id = ec.event_id and ec.sport_category_id = eoi.sport_category_id"
      )
      .groupBy("eoi.sport_category_id")
      .as("selected_slots");

    knex("eventCategories as ec")
      .where("ec.event_id", parseInt(req.body.eventId))
      .select("ec.sport_category_id", "ec.costs", "ec.total_slots", subcolumn)
      .groupBy("ec.sport_category_id")
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.EventCostDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: rows,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/saveUserInterest", function (req, res) {
  try {
    if (req.body.interested === true) {
      knex("user_event_interests")
        .insert({
          user_id: req.body.userId,
          event_id: req.body.eventId,
        })
        .then((rows) => {
          knex("user_event_interests as uei")
            .whereRaw("uei.event_id = " + req.body.eventId)
            .then((rows1) => {
              res.status(200).json({
                interestedCount: rows1.length,
                eventId: req.body.eventId,
                responseCode: 20000,
                message: constants.getResponseMessage(20000),
              });
            })
            .catch((err) => {
              res.status(400).json({
                responseCode: 40000,
                message: constants.getResponseMessage(40000),
              });
            });
        })
        .catch((err) => {
          res.status(400).json({
            responseCode: 40000,
            message: constants.getResponseMessage(40000),
          });
        });
    } else {
      knex("user_event_interests")
        .whereRaw("event_id = " + req.body.eventId)
        .andWhereRaw("user_id = " + req.body.userId)
        .del()
        .then((row) => {
          knex("user_event_interests as uei")
            .whereRaw("uei.event_id = " + req.body.eventId)
            .then((rows1) => {
              res.status(200).json({
                interestedCount: rows1.length,
                eventId: req.body.eventId,
                responseCode: 20000,
                message: constants.getResponseMessage(20000),
              });
            })
            .catch((err) => {
              res.status(400).json({
                responseCode: 40000,
                message: constants.getResponseMessage(40000),
              });
            });
        })
        .catch((err) => {
          res.status(400).json({
            responseCode: 40000,
            message: constants.getResponseMessage(40000),
          });
        });
    }
  } catch (err) {
    res.status(400).json({
      responseCode: 40000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/getCancellationCharges", function (req, res) {
  try {
    knex("cancellationCharges as cc")
      .where("cc.event_id", parseInt(req.body.eventId))
      .andWhere("cc.is_active", true)
      .select("cc.start_time", "cc.end_time", "cc.charges")
      .then((rows) => {
        var data = rows.map((x) =>
          mapper.map(x, mapper.CancellationChargesDBToUserModel)
        );
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(40000),
    });
  }
});

router.post("/cancelEventOrder", async function (req, res) {
  try {
    var eventOrderItemsPromise = knex.raw("update eventOrderItems as eoi set is_active =(?), selected_slots = (?), cancelled_slots = (eoi.selected_slots - ?) where item_id = ?",
      [parseInt(req.body.selected_slots) > 0, parseInt(req.body.selected_slots), parseInt(req.body.selected_slots), req.body.event_order_item_id]);
     
    var eventOrderMemberDetailsPromise = knex('eventregistration').update({ is_active: 0 })
      .whereIn("event_registration_id", req.body.members);

    var getEventPaymentOrderId = knex('eventorders').select("payment_order_id")
      .where("event_orders_id", req.body.event_order_id);

    var updateRefundAmount = knex('eventorders').update({ "refund_amount": req.body.refundAmount})
      .where("event_orders_id", req.body.event_order_id);

    Promise.all([eventOrderItemsPromise, eventOrderMemberDetailsPromise, getEventPaymentOrderId, updateRefundAmount])
      .then((result) => {
        try{
        var orderId = result[2].pop().payment_order_id;
        paymentHelper.refund(req.body.refundAmount, orderId)
        .then((result) => {
          res.status(200).json(result.responseObject.body);
        })
        .catch((err) => {
          res
            .status(400)
            .json({ message: "Error in iniating the refund of payment" });
        });;
      }
      catch (err){
        console.log(err);
        res
        .status(400)
        .json({ message: "Error in iniating the refund of payment2" });
      }
      });
  }
  catch (err) {
    console.log(err);
    res
    .status(400)
    .json({ message: "Error in iniating the refund of payment2" });
  }

});

router.post("/getEventOrderDetails", middleware, function (req, res) {
  try {

    var result = {};
    var userId = req.userId;
    var subCategoryNames = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.name)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_names");

    var subCategoryIds = knex("eventcategories as ec")
      .select(knex.raw("group_concat(sc.sport_category_id)"))
      .innerJoin(
        "sport_category as sc",
        "sc.sport_category_id",
        "ec.sport_category_id"
      )
      .whereRaw("ec.event_id = se.event_id")
      .as("sport_category_ids");

    var registeredCount = knex("eventOrderItems as eoi")
      .select(knex.raw("sum(eoi.selected_slots)"))
      .innerJoin(
        "eventOrders as eo",
        "eo.event_orders_id",
        "eoi.event_order_id"
      )
      .whereRaw("eo.event_id = se.event_id")
      .as("registered_count");

    var interestedCount = knex("user_event_interests as uei")
      .select(knex.raw("count(*)"))
      .whereRaw("uei.event_id = se.event_id")
      .as("interested_count");

    var eventsPromise = knex("sportevents as se")
      .leftJoin("sports as s", "s.sport_id", "se.sport_id")
      .select(
        "se.event_id",
        "se.event_name",
        "se.event_location",
        "se.event_start_date_time",
        "s.sport_id",
        "s.name as sport_Name",
        "se.submit_registered_user_details_by",
        "se.last_date_time_registration",
        knex.raw(
          "(select end_time from cancellationcharges where event_id = se.event_id and is_active = 1 order by end_time desc limit 1) as cancel_before"
        ),
        subCategoryNames,
        subCategoryIds,
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_primary = 1 limit 1) as file_path"
        ),
        knex.raw(
          "(select ei.file_path from eventImages ei where ei.event_id = se.event_id and ei.is_header = 1 limit 1) as header_file_path"
        ),

        interestedCount,
        registeredCount,
        knex.raw(
          "Exists(select 1 from user_event_interests uei where uei.event_id = se.event_id and uei.user_id = ? ) as interested",
          parseInt(userId)
        )
      )
      .whereRaw(
        "se.is_active = 1 and se.event_start_date_time > current_timestamp and se.event_id = (select event_id from eventOrders where event_orders_id = ?)",
        parseInt(req.body.eventOrderId)
      );

    var eventOrderItemDetailsPromise = knex('sportEvents as se')
      .join('eventOrders as eo', 'eo.event_id', 'se.event_id')
      .join('eventOrderItems as eoi', 'eoi.event_order_id', 'eo.event_orders_id')
      .join('sport_category as sc', 'sc.sport_category_id', 'eoi.sport_category_id')
      .join('eventCategories as ec', 'ec.sport_category_id', 'sc.sport_category_id')
      .select(['eoi.item_id', 'sc.sport_category_id', 'sc.name', 'ec.costs', 'ec.total_slots', 'eoi.selected_slots', 'eoi.total_amount']).where('eoi.event_order_id', parseInt(req.body.eventOrderId))
      .where('eoi.event_order_id', parseInt(req.body.eventOrderId));

    Promise.all([eventsPromise, eventOrderItemDetailsPromise])
      .then((values) => {
        result = mapper.map(values[0].pop(), mapper.EventGalleyDBToUserModel);
        result.Items = values[1].map((x) => {
          return mapper.map(x, mapper.EventCostDBToUserModel);
        });
        res.status(200).json({
          responseCode: 20000,
          message: constants.getResponseMessage(20000),
          data: result
        });
      })
      .catch((err) => {
        res.status(400).json({
          responseCode: 50000,
          message: constants.getResponseMessage(40000),
        });
      });
  } catch (err) {
    res.status(400).json({
      responseCode: 50000,
      message: constants.getResponseMessage(40000),
    });
  }
});

module.exports = router;
