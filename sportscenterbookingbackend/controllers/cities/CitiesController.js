var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var knex = require("../../knex");

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get("/cities", (req, res) => {
  try {
    knex("cities")
      .select("*")
      .then(rows => {
        res
          .status(200)
          .json({ message: "Cities fetched successfully", cities: rows });
      })
      .catch(err => {
        res
          .status(500)
          .json({ message: "Some problem in fetching the cities." });
      });
  } catch (err) {
    return res.status(400).json({ message: "Error" });
  }
});

module.exports = router;
