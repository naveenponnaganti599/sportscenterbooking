var express = require('express');
var app = express();
global.__root = __dirname + '/controllers/';

var testController = require(__root + 'test/TestController');
app.use('/api', testController);

var userController = require(__root + 'users/UserController');
app.use('/api/users', userController);

var citiesController = require(__root + 'cities/CitiesController');
app.use('/api', citiesController);

var sportsController = require(__root + 'sports/SportsController');
app.use('/api/sports', sportsController);

var eventsController = require(__root + 'events/EventsController');
app.use('/api/events', eventsController);

var paymentController = require(__root + 'payments/PaymentController');
app.use('/api', paymentController);

app.use(express.static(__dirname + '/public'));

var server = require('http').Server(app);

module.exports = server ;