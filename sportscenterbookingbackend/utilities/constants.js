var responses = {
    successResponses: [
        {
            responseCode: 20000,
            message: "Success"
        },
        {
            responseCode: 20001,
            message: "User created successfully"
        },
        {
            responseCode: 20002,
            message: "OTP sent successfully"
        },
        {
            responseCode: 20003,
            message: "OTP verified successfully"
        },
        {
            responseCode: 20004,
            message: "Order completed successfully"
        },
        {
            responseCode: 20005,
            message: "User details updated successfully"
        },
    ],
    clientSideErrorResponses: [
        {
            responseCode: 40000,
            message: "Invalid data"
        },
        {
            responseCode: 40001,
            message: "Invalid details. Signup Failed"
        },
        {
            responseCode: 40002,
            message: "Failed to send OTP. Please try again later"
        },
        {
            responseCode: 40003,
            message: "User with phone number doesn't exist"
        },
        {
            responseCode: 40004,
            message: "Invalid OTP"
        }
    ],
    serverSideErrorResponses: [
        {
            responseCode: 50000,
            message: "Server Error"
        }
    ]
};

var constants = {};

constants.getResponseMessage = function(respCode) {
    var index = responses.successResponses.findIndex(x=> x.responseCode == respCode);
    if(index > -1){
        return responses.successResponses[index].message;
    } 
    
    index = responses.clientSideErrorResponses.findIndex(x=> x.responseCode == respCode);
    if(index > -1) {
        return responses.clientSideErrorResponses[index].message;
    }

    index = responses.serverSideErrorResponses.findIndex(x=> x.responseCode == respCode);
    if(index > -1){
        return responses.serverSideErrorResponses[index].message;
    }
}

module.exports = constants;