var mapper = {};
var objectMapper = require('object-mapper');

mapper.UsersDBToUserModel = {
    // src : dest
    // src db model name
    // dest user model name
    "firstName": "firstName",
    "lastName": "lastName",
    "email": "email",
    "mobile": "mobile",
    "gender": "gender",
    "dob": "dateOfBirth",
    "bloodGroup": "bloodGroup",
    "tShirtSize": "tShirtSize",
    "status": "status",
    "emergency_contact_name": "emergencyContactName",
    "emergency_contact_number": "emergencyContactNumber",
    "role": "role",
    "created_at": "createdAt",
    "updated_at": "UpdatedAt",
    "id": "userId",
    "city_id": "cityId"
};

mapper.UsersUserToDBModel = {
    "firstName": "firstname",
    "lastName": "lastname",
    "email": "email",
    "mobile": "mobile",
    "gender": "gender",
    "dateOfBirth": "dob",
    "bloodGroup": "bloodGroup",
    "tShirtSize": "tShirtSize",
    "status": "status",
    "emergencyContactName": "emergency_contact_name",
    "emergencyContactNumber": "emergency_contact_number",
    "createdAt": "created_at",
    "userId": "id"
};

mapper.CitiesDBToUserModel = {
    "city_id": "cityId",
    "name": "name",
    "code": "cityCode"
};

mapper.SportsDBToUserModel = {
    "sport_id": "sportId",
    "name": "name",
    "description": "description",
};

mapper.SportCategoriesDBToUserModel = {
    "sport_category_id": "sportCategoryId",
    "sport_id": "sportId",
    "name": "name",
    "description": "description",
};

mapper.TShirtSizeDBToUserModel = {
    "size_id": "tShitSizeId",
    "name": "name"
};

mapper.GenderDBToUserModel = {
    "gender_id": "genderId",
    "name": "name"
};

mapper.BloodGroupDBToUserModel = {
    "group_id": "bloodGroupId",
    "group_name": "name"
};

mapper.BannerDBToUserModel = {
    "event_name": "eventName",
    "address": "address",
    "event_date": "eventDate",
    "sport_name": "sportName",
    "image_path": "imagePath",
    "sport_category_names": "sportCategoryNames?",
    "event_id": "eventId"
};

mapper.DashboardRegistrationsDBToUserModel = {
    "event_id": "eventId",
    "event_name": "eventName",
    "event_start_date_time": "eventStartDateTime",
    "selected_slots": "selectedSlots",
    "sport_category_id": "sportCategoryId",
    "name": "sportCategoryName",
    "event_venue": "eventVenue",
    "event_orders_id": "eventOrderId",
    "cancelled": "cancelled"
};

mapper.EventGalleyDBToUserModel = {
    "event_name": "eventName",
    "event_location": "eventLocation",
    "event_start_date_time": "eventStartDateTime",
    "sport_Name": "sportName",
    "file_path": "imagePath",
    "header_file_path": "headerImagePath",
    "sport_category_names": "sportCategoryNames?",
    "sport_category_ids": "sportCategoryIds?",
    "registered_count": "registeredCount",
    "interested_count": "interestedCount",
    "interested" : "interested",
    "sport_id": "sportId",
    "event_id": "eventId",
    "submit_registered_user_details_by": "submitUserDetailsBy",
    "last_date_time_registration": "registrationLastDate",
    "cancel_before": "cancelBefore"
};

mapper.SupportContactInfoDBToUserModel = {
    "email": "email",
    "whatsapp": "whatsapp",
    "contact_number": "contactNumber"
};

mapper.EventDetailsDBToUserModel = {
    "header": "header",
    "body": "body",
    "display_order": "displayOrder"
};

mapper.EventCostDBToUserModel = {
    "sport_category_id": "sportCategoryId",
    "name": "sportCategoryName",
    "costs": "costOfEachSlots",
    "total_slots": "totalAvailableSlots",
    "selected_slots": "selectedSlots",
    "item_id": "itemId",
    "total_amount": "itemTotalAmount"
}

mapper.EventOrderUserToDBModel = {
    "eventId": "event_id",
    "totalAmount": "total_amount",
    "couponApplied": "coupon_applied",
    "discountReceived": "discount_received",
    "amountPayable": "amount_payable",
    "paymentId": "payment_id",
    "createdByUserId": "created_by_user_id",
    "createdAt": "created_at?",
    "updatedAt": "updated_at?",
    "isActive": "is_active?",
    "paymentOrderId": "payment_order_id?"
}

mapper.EventOrderItemUserToDBModel = {
    "eventOrderId": "event_order_id",
    "sportCategoryId": "sport_category_id",
    "selectedSlots": "selected_slots",
    "isActive": "is_active"
}

mapper.EventOrderMemberDetailsUserToDBModel = {
    "eventOrderId": "event_order_id",
    "firstName": "first_name",
    "lastName": "last_name",
    "phoneNumber": "phone_number",
    "email": "email",
    "dateOfBirth": "dob",
    "genderId": "gender_id",
    "tShirtSizeId": "tshirt_size",
    "isActive": "is_active",
    "isSelf": "is_self",
    "eventCategoryId": "event_category_id",
    "bloodGroupId": "bloodGroupId",
    "emergencyContactNumber": "emergency_contact_number",
    "emergencyContactName": "emergency_contact_name"
}

mapper.EventOrderMemberDetailsDBToUserModel = {
    "event_registration_id": "eventRegistrationId",
    "event_order_id": "eventOrderId",
    "first_name": "firstName",
    "last_name": "lastName",
    "phone_number": "phoneNumber",
    "email": "email",
    "dob": "dateOfBirth",
    "gender_id": "genderId",
    "tshirt_size": "tShirtSizeId",
    "is_active": "isActive",
    "is_self": "isSelf",
    "event_category_id": "eventCategoryId",
    "sport_caegory_id": "sportCategoryId",
    "costs": "costOfEachSlots",
    "bloodGroupId": "bloodGroupId",
    "emergency_contact_name": "emergencyContactName",
    "emergency_contact_number": "emergencyContactNumber"
}

mapper.CancellationChargesDBToUserModel = {
    "start_time": "startTime",
    "end_time": "endTime",
    "charges": "charges"
}

mapper.map = function (src, map) {
    return objectMapper(src, map);
}

module.exports = mapper;
