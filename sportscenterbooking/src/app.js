import React, { useState } from 'react';
import { Image, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';

import SplashScreen from './screens/splash-screen';
import SignUpScreen from './screens/sign-up';
import SignInScreen from './screens/sign-in';
import OTPEntryScreen from './screens/otp-entry';
import ProfileScreen from './screens/profile';
import DashboardNavigator from './navigators/dashboard-stack';
import DashboardScreen from './screens/dashboard';
import CourtScreen from './screens/court';
import EventsScreen from './screens/events';
import PreferredCenters from './components/preferred-centers';
import { SIGN_IN_SCREEN, SIGN_UP_SCREEN, OTP_ENTRY_SCREEN, PROFILE_SCREEN, DASHBOARD_NAVIGATOR, COURT_SCREEN, EVENTS_SCREEN, EVENTS_NAVIGATOR } from './constants/navigation';


import ImgHomeBlue from './assets/images/img-home-blue.png';
import ImgHomeGrey from './assets/images/img-home-grey.png';
import ImgCourtGrey from './assets/images/img-court-grey.png';
import ImgTrophyGrey from './assets/images/img-trophy-grey.png';
import ImgTrophyBlue from './assets/images/img-trophy-blue.png';
import { AUTH_TOKEN } from './constants/async-storage';
import { updateToken } from './screens/otp-entry/otp-entry.actions';
import EventsStack from './navigators/events-stack';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const App = function ({ isLoading, authToken, updateAuthToken }) {

    React.useEffect(() => {
        const bootstrapAsync = async () => {
            let token = await AsyncStorage.getItem(AUTH_TOKEN)
            if (token) {
                updateAuthToken(token);
            }
        }
        bootstrapAsync();
    }, []);

    const [userToken, setUserToken] = useState(false);

    const updateUserToken = () => {
        setUserToken(true)
    }

    const tabNavigatorOptions = ({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === DASHBOARD_NAVIGATOR) {
                iconName = focused
                    ? ImgHomeBlue
                    : ImgHomeGrey;
            } else if (route.name === COURT_SCREEN) {
                iconName = focused
                    ? ImgCourtGrey
                    : ImgCourtGrey
            } else if (route.name === EVENTS_NAVIGATOR) {
                iconName = focused
                    ? ImgTrophyBlue
                    : ImgTrophyGrey
            }
            return <Image
                source={iconName}
                size={size}
                color={color}
                style={styles.tabBarIcon} />;
        },
        tabBarLabel: () => null
    })

    if (isLoading) {
        return <SplashScreen />
    }
    return (
        <NavigationContainer>
            {
                !authToken ?
                    <Stack.Navigator
                        headerMode='none' >
                        <Stack.Screen
                            name={SIGN_IN_SCREEN}
                            component={SignInScreen}
                        />
                        <Stack.Screen
                            name={OTP_ENTRY_SCREEN}
                            component={OTPEntryScreen}
                            initialParams={{
                                updateUserToken: updateUserToken
                            }} />
                        <Stack.Screen
                            name={SIGN_UP_SCREEN}
                            component={SignUpScreen} />
                        <Stack.Screen
                            name={PROFILE_SCREEN}
                            component={ProfileScreen} />
                    </Stack.Navigator>
                    :
                    <Tab.Navigator
                        screenOptions={tabNavigatorOptions}>
                        <Tab.Screen name={DASHBOARD_NAVIGATOR} component={DashboardNavigator} />
                        <Tab.Screen name={COURT_SCREEN} component={CourtScreen} />
                        <Tab.Screen name={EVENTS_NAVIGATOR} component={EventsStack} />
                    </Tab.Navigator>
            }
        </NavigationContainer>
    )
}

const mapStateToProps = (state) => {
    return {
        authToken: state.user.authToken
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateAuthToken: (token) => dispatch(updateToken(token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
    tabBarIcon: {
        resizeMode: 'contain',
        height: 25,
        width: 25
    }
})