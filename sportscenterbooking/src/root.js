import React from 'react';
import { Provider } from 'react-redux';

import createStore from './redux/store';
import App from './app';

const store = createStore();

export default function Root() {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    )
}