import axios from 'axios';

import endpoints from '../constants/endpoints';
import { REQUEST_TIME_OUT_ERROR_MESSAGE } from '../constants/strings';

export default axios.create({
    baseURL: endpoints.BASE_URL,
    timeout: 5000,
    timeoutErrorMessage: REQUEST_TIME_OUT_ERROR_MESSAGE,
    headers: {
        "Content-Type": "application/json"
    }
});