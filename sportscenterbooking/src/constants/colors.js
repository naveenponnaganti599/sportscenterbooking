export const COLOR_WHITE = "#FFFFFF";
export const COLOR_BLACK = "#000000";
export const COLOR_BLUE = "#4285F4";
export const COLOR_GREY_1 = "#C4C4C4";
export const COLOR_GREY_2 = "#E5E5E5";
export const COLOR_TRANSPARENT = 'transparent';

export const COLOR_TEXT = COLOR_GREY_1
export const COLOR_PRIMARY = COLOR_BLUE;
export const COLOR_TEXT_PRIMARY_BUTTON = COLOR_WHITE;
export const COLOR_BORDER_HIGHLIGHT = COLOR_BLACK;
export const COLOR_BORDER = COLOR_GREY_2;
