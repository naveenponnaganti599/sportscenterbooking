export default {
    BASE_URL: "https://swyng-api.herokuapp.com",
    POST_SEND_OTP: "/api/users/sendOTP",
    POST_VALIDATE_OTP: "/api/users/validateOTP",
    POST_SIGN_UP: "/api/users/signUp"
}