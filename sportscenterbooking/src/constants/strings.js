export const VERSION = "0.0.1";
export const ANDROID = "android";
export const IOS = "ios";
export const SHOW_DETAILS = 'Show Details';
export const REQUEST_TIME_OUT_ERROR_MESSAGE = "Request timed out";