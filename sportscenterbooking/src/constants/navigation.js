export const SPLASH_SCREEN = "SPLASH_SCREEN";
export const SIGN_IN_SCREEN = "SIGN_IN_SCREEN";
export const SIGN_UP_SCREEN = "SIGN_UP_SCREEN";
export const OTP_ENTRY_SCREEN = "OTP_ENTRY_SCREEN";
export const HOME_SCREEN = "HOME_SCREEN";
export const PROFILE_SCREEN = "PROFILE_SCREEN";
export const COURT_SCREEN = "COURT_SCREEN";


export const DASHBOARD_NAVIGATOR = "DASHBOARD_NAVIGATOR";
export const DASHBOARD_SCREEN = "DASHBOARD_SCREEN";
export const MENU_SCREEN = "MENU_SCREEN";

export const EVENTS_NAVIGATOR = "EVENTS_NAVIGATOR";
export const EVENTS_SCREEN = "EVENTS_SCREEN";