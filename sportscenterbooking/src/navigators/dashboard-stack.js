import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { DASHBOARD_SCREEN, MENU_SCREEN } from '../constants/navigation';
import DashboardScreen from '../screens/dashboard';
import MenuScreen from '../screens/menu';

const Stack = createStackNavigator();

export default function DashboardStack() {
    return (
        <Stack.Navigator
            headerMode="screen" >
            <Stack.Screen
                name={DASHBOARD_SCREEN}
                component={DashboardScreen} />
            <Stack.Screen name={MENU_SCREEN}
                component={MenuScreen} />
        </Stack.Navigator>
    )
}