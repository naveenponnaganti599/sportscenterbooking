import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import EventsScreen from '../screens/events';
import { EVENTS_SCREEN, MENU_SCREEN } from '../constants/navigation';
import MenuScreen from '../screens/menu';


const Stack = createStackNavigator();

export default function EventsStack() {
    return (
        <Stack.Navigator headerMode="screen">
            <Stack.Screen component={EventsScreen} name={EVENTS_SCREEN} />
            <Stack.Screen component={MenuScreen} name={MENU_SCREEN} />
        </Stack.Navigator>
    )
}