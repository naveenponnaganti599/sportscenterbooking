import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { COLOR_WHITE, COLOR_PRIMARY, COLOR_TEXT } from '../constants/colors';


const DetailRow = function ({ leftTopText, leftBottomText, rightTopText, rightBottomText }) {
    return (
        <View style={styles.detailRow}>
            <View style={styles.leftDetail}>
                {leftTopText && <Text style={styles.detailText}>{leftTopText}</Text>}
                {leftBottomText && <Text style={styles.detailText}>{leftBottomText}</Text>}
            </View>
            <View style={styles.rightDetail}>
                {rightTopText && <Text style={styles.detailText}>{rightTopText}</Text>}
                {rightBottomText && < Text style={styles.detailText}>{rightBottomText}</Text>}
            </View>
        </View >
    )
}

export default function UpcomingCard({ data, title, leftTopKey, leftBottomKey, rightTopKey, rightBottomKey }) {
    return (
        <View style={styles.cardContainer}>
            <View style={styles.leftBanner} />
            <View style={styles.contentContainer}>
                <Text style={styles.upcomingText}>{title}</Text>
                {
                    data.map((item, index) => {
                        return (
                            <DetailRow key={index}
                                leftTopText={leftTopKey && item[leftTopKey]}
                                leftBottomText={leftBottomKey && item[leftBottomKey]}
                                rightTopText={rightTopKey && item[rightTopKey]}
                                rightBottomText={rightBottomKey && item[rightBottomKey]}
                            />
                        )
                    })
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: COLOR_WHITE,
        elevation: 4,
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: {
            height: 4,
            width: 4
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        borderRadius: 10,
        width: '100%',
        margin: 10,
        marginTop: 0,
        flexDirection: 'row',
        flex: 1
    },
    leftBanner: {
        backgroundColor: COLOR_PRIMARY,
        flex: 0.05,
        height: 5,
        marginTop: 10,
        minWidth: 7,
        maxWidth: 30,
        marginRight: 6
    },
    contentContainer: {
        flex: 1,
        padding: 5
    },
    upcomingText: {
        color: 'rgba(0,0,0,1)',
        fontSize: 18
    },
    detailRow: {
        flexDirection: 'row',
        flex: 1,
        paddingVertical: 5
    },
    leftDetail: {
        flex: 3,
        paddingRight: 5
    },
    rightDetail: {
        flex: 5
    },
    detailText: {
        color: 'rgba(0,0,0,0.75)',
        fontSize: 16
    }
})