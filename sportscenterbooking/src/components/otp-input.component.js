import React from 'react';
import { View, StyleSheet } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { COLOR_TEXT, COLOR_BORDER_HIGHLIGHT, COLOR_BLACK } from '../constants/colors';

export default function OTPInput({ digitCount, setOTP }) {
    return (
        <OTPInputView
            style={styles.container}
            pinCount={digitCount}
            onCodeChanged={setOTP}
            codeInputHighlightStyle={styles.highlightedInput}
            codeInputFieldStyle={styles.inputStyle}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        maxWidth: 250,

    },
    highlightedInput: {
        borderColor: COLOR_BORDER_HIGHLIGHT
    },
    inputStyle: {
        color: COLOR_BLACK,
        fontSize: 20,
        borderRadius: 10
    }
})