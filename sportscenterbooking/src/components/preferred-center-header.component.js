import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import {
  COLOR_BORDER,
  COLOR_WHITE,
  COLOR_TEXT,
  COLOR_TRANSPARENT,
  COLOR_PRIMARY,
} from '../constants/colors';

export default class PreferredCenterHeader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{this.props.item.title}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 36,
    backgroundColor: COLOR_BORDER,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingLeft: 8
  },
});
