import React, { useState } from 'react';
import { View } from 'react-native';
import { Picker } from '@react-native-community/picker';

export default function CustomPicker({ items, selectedValue, onChange, boxStyle, itemStyle, placeholder }) {

    const onSelectionChangeAndroid = function (value, index) {
        onChange(value, index);
    }

    const pickerItems = items.map(function (item, index) {
        return (
            <Picker.Item key={index} value={item.value} label={item.label} />
        )
    });

    return (
        <View style={boxStyle}>
            <Picker
                style={itemStyle}
                itemStyle={itemStyle}
                selectedValue={selectedValue.value}
                onValueChange={onSelectionChangeAndroid}>
                <Picker.Item value="" label={placeholder} />
                {
                    pickerItems
                }
            </Picker>
        </View>
    )
}