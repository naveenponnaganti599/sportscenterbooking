import React, { useState } from 'react';
import { Picker } from '@react-native-community/picker';
import { Text, TouchableOpacity, View } from 'react-native';

export default function CustomPickerIOS({ items, selectedValue, onChange, boxStyle, itemStyle }) {

    const [showPicker, setShowPicker] = useState(false);

    const onSelectionChange = (value, index) => {
        onChange(value, index);
        setShowPicker(false);
    }

    const onTouchablePress = function () {
        setShowPicker(true);
    }

    return (
        <View>
            <TouchableOpacity style={boxStyle} onPress={onTouchablePress}>
                <Text style={itemStyle}>
                    {selectedValue.label}
                </Text>
            </TouchableOpacity>

            {showPicker &&
                <Picker selectedValue={selectedValue.value} onValueChange={onSelectionChange}>
                    {
                        items.map((item, index) => {
                            return (
                                <Picker.Item key={index} value={item.value} label={item.label} />
                            )
                        })
                    }
                </Picker>
            }
        </View>
    )
}