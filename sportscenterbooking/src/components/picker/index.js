import { Platform } from 'react-native';
import PickerAndroid from './picker-android';
import PickerIOS from './picker-ios';

export default Platform.OS == "android" ? PickerAndroid : PickerIOS;