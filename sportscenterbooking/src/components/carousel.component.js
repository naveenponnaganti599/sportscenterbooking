import React, { useState } from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { COLOR_PRIMARY, COLOR_GREY_1 } from '../constants/colors';

const getRenderItem = ({ item, index }) => {
    return (
        <View style={styles.card}>
            <Image source={item.image} style={styles.image} />
            <View style={styles.cardTextContainer}>
                <Text style={styles.sportName}>{item.sportName}</Text>
                <Text style={styles.eventName}>{item.eventName}</Text>
                <Text style={styles.time}>{item.time}</Text>
                <Text style={styles.type}>{item.type}</Text>
            </View>
        </View>
    )
}

const { width, height } = Dimensions.get('window');


export default function CustomCarousel({ data }) {
    const [activeSlide, setActiveSlide] = useState(0);


    return (
        <View style={styles.carouselContainer}>
            <Carousel
                data={data}
                sliderWidth={width - 8}
                itemWidth={width - 8}
                renderItem={getRenderItem}
                onSnapToItem={setActiveSlide}
            />
            <Pagination
                dotsLength={data.length}
                activeDotIndex={activeSlide}
                containerStyle={styles.paginationContainer}
                dotStyle={styles.activeDot}
                inactiveDotStyle={styles.inactiveDot}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    carouselContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    activeDot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        backgroundColor: COLOR_PRIMARY
    },
    inactiveDot: {
        width: 8,
        height: 8,
        borderRadius: 5,
        marginHorizontal: 8,
        backgroundColor: COLOR_GREY_1
    },
    paginationContainer: {
        maxWidth: 30,
        height: 0
    },
    image: {
        width: '100%',
        resizeMode: 'stretch',
    },
    card: {
        marginBottom: 0,
        marginHorizontal: 10,
        borderRadius: 2,
    },
    list: {
        flex: 1
    },
    cardTextContainer: {
        position: 'absolute',
        bottom: 1,
        padding: 15
    },
    sportName: {
        fontSize: 14,
        lineHeight: 16
    },
    eventName: {
        fontSize: 18,
        lineHeight: 21
    },
    time: {
        fontSize: 14,
        lineHeight: 16,
        fontWeight: '500'
    },
    type: {
        fontSize: 14,
        lineHeight: 16
    }
})