import React from 'react';
import { Image, StyleSheet } from 'react-native';

export default function TabBarIcon({ color, size, image }) {
    return (
        <Image source={image}
            color={color}
            size={size}
            style={styles.icon} />
    )
}

const styles = StyleSheet.create({
    icon: {
        resizeMode: 'contain',
        height: 25,
        width: 25
    }
})