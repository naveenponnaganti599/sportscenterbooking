import React, { Component } from 'react';
import { AppRegistry, SectionList, StyleSheet, Text, View, ScrollView } from 'react-native';
import PreferredCenterCard from '../preferred-center-card.component';
import PreferredCenterHeader from '../preferred-center-header.component';
import { COLOR_TRANSPARENT } from '../../constants/colors';

export default class PreferredCenters extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <SectionList
            sections={[
              {
                title: 'Preferred Centers',
                data: [
                  {
                    centerId: 1,
                    centerName: 'Center1',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 2,
                    centerName: 'Center2',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 3,
                    centerName: 'Center3',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 4,
                    centerName: 'Center4',
                    centerAddress: 'Road number 1, City, State.',
                  },
                ],
              },
              {
                title: 'Other Centers',
                data: [
                  {
                    centerId: 5,
                    centerName: 'Center5',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 6,
                    centerName: 'Center6',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 7,
                    centerName: 'Center7',
                    centerAddress: 'Road number 1, City, State.',
                  },
                  {
                    centerId: 8,
                    centerName: 'Center8',
                    centerAddress: 'Road number 1, City, State.',
                  },
                ],
              },
            ]}
            renderItem={({ item }) => <PreferredCenterCard item={item}></PreferredCenterCard>}
            renderSectionHeader={({ section }) => <PreferredCenterHeader item={section}></PreferredCenterHeader>}
            keyExtractor={(item, index) => index}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_TRANSPARENT,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 22,
    fontWeight: 'bold',
    color: '#fff',
    backgroundColor: '#8fb1aa',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
