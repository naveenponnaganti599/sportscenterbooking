import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import {
  COLOR_BORDER,
  COLOR_WHITE,
  COLOR_TEXT,
  COLOR_TRANSPARENT,
  COLOR_PRIMARY,
} from '../constants/colors';

export default class PreferredCenterCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{this.props.item.centerName}</Text>
        <Text style={styles.font14}>{this.props.item.centerAddress}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLOR_TRANSPARENT,
    borderBottomColor: COLOR_BORDER,
    borderBottomWidth: 1,
    padding: 5,
    paddingLeft: 16
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  font14: {
    fontSize: 14,
  },
});
