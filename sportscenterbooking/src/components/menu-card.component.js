import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native';

export default function MenuCard({ text, index }) {
    return (
        <View style={styles.cardContainer} key={index}>
            <TouchableOpacity style={styles.card}>
                <Text numberOfLines={2}
                    style={styles.cardText}>{text}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        flex: 1,
        margin: 5,
        ...Platform.select({
            android: {
                overflow: 'hidden',
                paddingBottom: 6,
                paddingHorizontal: 3,
                borderRadius: 10
            }
        })
    },
    card: {
        paddingVertical: 25,
        paddingHorizontal: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 6,
        shadowOffset: {
            height: 0.6 * 6
        },
        shadowOpacity: 0.0015 * 6 + 0.18,
        shadowRadius: 0.5 * 6,
        shadowColor: 'rgba(0,0,0,1)'
    },
    cardText: {
        textAlign: 'center'
    }
})