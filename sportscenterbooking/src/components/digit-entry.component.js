import React from 'react';
import {
    TextInput,
    StyleSheet
} from 'react-native';
import { COLOR_BORDER, COLOR_TEXT } from '../constants/colors';

export default function DigitEntry({ isFocussed, onFocus }) {
    return (
        <TextInput
            clearTextOnFocus={true}
            // onFocus={() => { }}
            selectionColor='red'
            style={styles.textInput}
            keyboardType="number-pad"
            isFocussed={true}
            maxLength={1}
        />
    )
}

const styles = StyleSheet.create({
    textInput: {
        borderColor: COLOR_BORDER,
        borderWidth: 1,
        height: 40,
        width: 40,
        marginHorizontal: 10,
        color: COLOR_TEXT,
        fontSize: 18,
        padding: 0,
        textAlign: 'center',
        textAlignVertical: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
})