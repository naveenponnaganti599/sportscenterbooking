import React, {Component} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import {
  COLOR_BORDER,
  COLOR_WHITE,
  COLOR_TEXT,
  COLOR_TRANSPARENT,
  COLOR_PRIMARY,
} from '../constants/colors';
import {SHOW_DETAILS} from '../constants/strings';

export default class EventCard extends Component {
  constructor(props) {
    super(props);
  }

  click = clickable => {
    this.props.press(this.props.item);
  };

  showDetailsClicked = () => {
    this.props.press(this.props.item);
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={[
            styles.coverImage,
            this.props.isDashboard ? {height: 450} : {height: 550},
          ]}
          source={this.props.item.image}></Image>
        <View style={styles.textContainer}>
          <Text style={styles.font14}>{this.props.item.sport}</Text>
          <Text style={styles.title}>{this.props.item.eventName}</Text>
          <Text style={styles.font14}>
            {this.props.item.date}, {this.props.item.venue}
          </Text>
          <Text style={styles.font12}>{this.props.item.description}</Text>
          {!this.props.isDashboard ? (
            <TouchableOpacity
              style={styles.detailButton}
              onPress={this.showDetailsClicked}>
              <Text style={styles.buttonText}>{SHOW_DETAILS}</Text>
            </TouchableOpacity>
          ) : (
            <View></View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: COLOR_TRANSPARENT,
  },
  coverImage: {
    flex: 1,
    resizeMode: 'cover',
    marginTop: 10,
    marginBottom: 10,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 10,
    borderColor: COLOR_BORDER,
    borderWidth: 1,
    height: 450,
    elevation: 10,
  },
  textContainer: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 1,
    backgroundColor: COLOR_WHITE,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 20,
    marginLeft: 20,
    padding: 10,
    elevation: 10,
    borderRadius: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  font14: {
    fontSize: 14,
  },
  font12: {
    fontSize: 12,
  },
  detailButton: {
    margin: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 4,
    paddingBottom: 4,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: COLOR_PRIMARY,
    alignSelf: 'center',
  },
  buttonText: {
    color: COLOR_WHITE,
    fontSize: 14,
  },
});