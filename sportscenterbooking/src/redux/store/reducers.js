import { combineReducers } from 'redux'
import actionTypes from '../actions/action-types';

import userReducer from './user-reducer';
import citiesReducer from './cities-reducer';

function sampleReducer(state = { sampleData: {} }, action) {
    switch (action.type) {
        case actionTypes.SAMPLE_ACTION:
            return { ...state, sampleData: action.payload };
        default: return state
    }
}

const rootReducer = combineReducers({
    sampleReducer: sampleReducer,
    user: userReducer,
    cities: citiesReducer,
});

export default rootReducer;