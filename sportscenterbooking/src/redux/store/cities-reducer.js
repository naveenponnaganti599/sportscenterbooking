import actionTypes from '../actions/action-types';

const initialState = {
    1: {
        id: 1,
        name: "Bengaluru"
    },
    2: {
        id: 2,
        name: "Chennai"
    },
    3: {
        id: 3,
        name: "Delhi NCR"
    },
    4: {
        id: 4,
        name: "Hyderabad"
    },
    5: {
        id: 5,
        name: "Pune"
    },
}

export default GlobalReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_CITIES:
            return {
                ...state,
                ...action.payload
            }
        default: return state
    }
}