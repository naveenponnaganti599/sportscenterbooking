import actionTypes from '../actions/action-types';

const initialState = {
    authToken: "",
    id: "",
    firstName: "",
    lastName: "",
    email: "",
    dateOfBirth: "",
    phoneNumber: "",
    bloodGroup: "",
    preferredCity: 3,
    tShirtSize: "",
    role: ""
}

export default UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_PHONE_NUMBER:
            return {
                ...state,
                phoneNumber: action.payload
            };
        case actionTypes.UPDATE_USER_DETAILS:
            return {
                ...state,
                ...action.payload
            };
        case actionTypes.UPDATE_AUTH_TOKEN:
            return {
                ...state,
                authToken: action.payload
            }
        case actionTypes.UPDATE_PREFERRED_CITY:
            return {
                ...state,
                preferredCity: action.payload
            }
        default: return state;
    }
}