import actions from "../actions/action-types";

export function sampleAction(samplePayload) {
    return {
        type: actions.SAMPLE_ACTION,
        payload: samplePayload
    }
}