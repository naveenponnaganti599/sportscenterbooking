import React from 'react';

import Sample from '../../components/sample.component';
import { SPLASH_SCREEN } from '../../constants/navigation';

export default function SplashScreen() {
    return <Sample screenName={SPLASH_SCREEN} />
}