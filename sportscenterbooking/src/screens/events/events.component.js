import React from 'react';

import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Image, Dimensions } from 'react-native';
import { EVENTS_SCREEN, MENU_SCREEN } from '../../constants/navigation';
import ImgEventBackground from '../../assets/images/img-event-background.png';
import ImgArrowRight from '../../assets/images/img-arrow-right.png';
import { COLOR_WHITE } from '../../constants/colors';

const { width, height } = Dimensions.get("screen");
export default function Events({ navigation }) {

    const header = () => {
        return (
            <View style={styles.headerContainer} >
                <SafeAreaView >
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => navigation.navigate(MENU_SCREEN)} >
                            <Image style={styles.headerLeftIcon} source={ImgArrowRight} />
                        </TouchableOpacity>
                    </View>
                </SafeAreaView >
            </View >
        )
    }
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: true,
            header: header
        })
    })
    return (
        <SafeAreaView style={styles.screen}>
            <View style={styles.screen} >
                <Image source={ImgEventBackground} style={{ resizeMode: 'stretch', height: '100%', width: width, margin: 4 }} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerContainer: {
        backgroundColor: COLOR_WHITE,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.25)'
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 54
    },
    headerLeftIcon: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        margin: 12
    },
})
