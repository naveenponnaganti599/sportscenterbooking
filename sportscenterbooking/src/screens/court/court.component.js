import React from 'react';

import { View, Text, StyleSheet } from 'react-native';
import { COURT_SCREEN } from '../../constants/navigation';

export default function Court({ }) {
    return (
        <View style={styles.screen}>
            <Text>{COURT_SCREEN}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})