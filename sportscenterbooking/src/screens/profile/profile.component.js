import React, { useState, useReducer } from 'react';
import {
    View, ScrollView, Text, TextInput,
    TouchableOpacity, Platform, Dimensions,
    StyleSheet, StatusBar, ActivityIndicator
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

import api from '../../services/api';
import Picker from '../../components/picker';
import Sample from '../../components/sample.component';
import { PROFILE_SCREEN, OTP_ENTRY_SCREEN } from '../../constants/navigation';
import { COLOR_WHITE, COLOR_BORDER, COLOR_TEXT, COLOR_PRIMARY, COLOR_TEXT_PRIMARY_BUTTON } from '../../constants/colors';
import { HEADING, SUB_HEADING, FIRST_NAME, LAST_NAME, TEN_DIGIT_NUMBER, PRIMARY_EMAIL, DATE_OF_BIRTH, GENDER, T_SHIRT_SIZE, BLOOD_GROUP, EMERGENCY_CONTACT_NUMBER, EMERGENCY_CONTACT_NAME, SUBMIT_BUTTON_TEXT, USER_CREATE_TEXT } from './profile.constants';
import { ANDROID } from '../../constants/strings';
import endpoints from '../../constants/endpoints';

const { height } = Dimensions.get('window');

export default function ProfileScreen({ navigation, user, saveProfileDetails }) {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState(new Date());
    const [gender, setGender] = useState({ label: GENDER, value: '' });
    const [bloodGroup, setBloodGroup] = useState('');
    const [tShirtSize, setTShirtSize] = useState({ label: T_SHIRT_SIZE, value: "" });
    const [emergencyContactName, setEmergencyContactName] = useState('');
    const [emergencyContactNumber, setEmergencyContactNumber] = useState('');

    const [showDatePicker, setShowDatePicker] = useState(false);
    const [isDOBEdited, setIsDOBEdited] = useState(false);

    const dobPressed = function () {
        setIsDOBEdited(true);
        setShowDatePicker(true);
    }

    const onDOBChange = function (event, selectedDate) {
        setShowDatePicker(Platform.OS == "ios");
        setDateOfBirth(selectedDate);
    }

    const onDOBDonePress = function () {
        setShowDatePicker(false);
    }

    const tShirtSizes = [
        { value: 'S', label: 'Small' },
        { value: 'M', label: 'Medium' },
        { value: 'L', label: 'Large' },
        { value: 'XL', label: 'X - Large' },
        { value: 'XXL', label: 'XX - Large' },
    ]
    const genderOptions = [
        { value: 'M', label: 'Male' },
        { value: 'F', label: 'Female' },
        { value: 'Ot', label: 'Others' }
    ]

    const onGenderChange = (value, index) => {
        setGender({ value: value, label: genderOptions[Platform.OS == "android" ? index - 1 : index].label });
    }

    const onShirtSizeChange = function (value, index) {
        setTShirtSize({ value: value, label: tShirtSizes[Platform.OS == "android" ? index - 1 : index].label });
    }

    const [isSubmitting, setIsSubmitting] = React.useState(false);

    const updateProfileDetails = async () => {
        let body = {
            user: {
                "firstname": firstName,
                "lastname": lastName,
                "email": email,
                "mobile": user.phoneNumber,
                "gender": gender.value,
                "dob": dateOfBirth,
                "bloodGroup": bloodGroup,
                // "tShirtSize": tShirtSize,
                "status": 1,
                "emergency_contact_name": emergencyContactName,
                "emergency_contact_number": emergencyContactNumber,
                "role": 1,
                "Created_at": new Date().toISOString()
            }
        };
        return api.post(endpoints.POST_SIGN_UP, body)
    }

    const sendOTP = async () => {
        let body = {
            phone: user.phoneNumber
        }
        return api.post(endpoints.POST_SEND_OTP, body);
    }

    const onSubmitPressed = function () {
        setIsSubmitting(true);
        updateProfileDetails()
            .then(response => {
                if (response.status == 200 && response.data.message == USER_CREATE_TEXT) {
                    saveProfileDetails(response.data.user);
                    sendOTP();
                    navigation.navigate(OTP_ENTRY_SCREEN);
                    setIsSubmitting(false);
                }
            })
            .catch(error => {
                if (error && error.response && error.response.status == 400 && error.response.data.message == USER_CREATE_TEXT) {
                    saveProfileDetails(error.response.data.user);
                    sendOTP();
                    navigation.navigate(OTP_ENTRY_SCREEN);
                    setIsSubmitting(false);
                }
            })
    }

    return (
        <ScrollView style={styles.scrollView} bounces={false}>
            <View style={styles.screen}>
                <Text style={styles.heading}>{HEADING}</Text>
                <Text style={styles.subHeading}>{SUB_HEADING}</Text>
                <View style={styles.inputsContainer}>
                    <TextInput
                        placeholder={FIRST_NAME}
                        style={[styles.input, styles.text]}
                        onChangeText={setFirstName}
                        value={firstName} />
                    <TextInput
                        placeholder={LAST_NAME}
                        style={[styles.input, styles.text]}
                        onChangeText={setLastName}
                        value={lastName} />
                    <TextInput
                        editable={false}
                        value={user.phoneNumber}
                        placeholder={TEN_DIGIT_NUMBER}
                        style={[styles.input, styles.text]} />
                    <TextInput
                        placeholder={PRIMARY_EMAIL}
                        style={[styles.input, styles.text]}
                        onChangeText={setEmail}
                        value={email} />
                    <TouchableOpacity
                        style={styles.input}
                        onPress={dobPressed}
                    >
                        <Text style={styles.text}>{isDOBEdited ? dateOfBirth.toDateString() : DATE_OF_BIRTH}</Text>
                    </TouchableOpacity>
                    {
                        showDatePicker
                        &&
                        (
                            <View>
                                <DateTimePicker
                                    value={dateOfBirth}
                                    mode={"date"}
                                    onChange={onDOBChange}
                                    display="default"
                                />
                                {
                                    Platform.OS == "ios" && <TouchableOpacity
                                        style={styles.dobDoneTouchable}
                                        onPress={onDOBDonePress}
                                    >
                                        <Text style={{ fontSize: 10, color: COLOR_TEXT_PRIMARY_BUTTON }}>Done</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                        )
                    }
                    <Picker
                        boxStyle={styles.input}
                        itemStyle={styles.text}
                        items={genderOptions}
                        onChange={onGenderChange}
                        selectedValue={gender}
                        placeholder={GENDER}
                    />
                    <TextInput
                        placeholder={BLOOD_GROUP}
                        style={[styles.input, styles.text]}
                        onChangeText={setBloodGroup}
                        value={bloodGroup} />
                    <Picker
                        boxStyle={styles.input}
                        itemStyle={styles.text}
                        items={tShirtSizes}
                        onChange={onShirtSizeChange}
                        selectedValue={tShirtSize}
                        placeholder={T_SHIRT_SIZE}
                    />
                    <TextInput
                        placeholder={EMERGENCY_CONTACT_NAME}
                        style={[styles.input, styles.text]}
                        onChangeText={setEmergencyContactName}
                        value={emergencyContactName} />
                    <TextInput
                        placeholder={EMERGENCY_CONTACT_NUMBER}
                        style={[styles.input, styles.text]}
                        onChangeText={setEmergencyContactNumber}
                        value={emergencyContactNumber} />
                    <TouchableOpacity disabled={isSubmitting} style={[styles.input, styles.submitButton]} onPress={onSubmitPressed}>
                        <Text style={[styles.text, styles.submitButtonText]}>{SUBMIT_BUTTON_TEXT}</Text>
                        {
                            isSubmitting && <ActivityIndicator />
                        }
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView >
    )
}

const styles = StyleSheet.create({
    scrollView: {
        flex: 1,
    },
    screen: {
        flex: 1,
        backgroundColor: COLOR_WHITE,
        minHeight: (
            Platform.OS == ANDROID
                ? height - StatusBar.currentHeight
                : height
        ),
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        flex: 1
    },
    heading: {
        marginTop: 40,
        fontSize: 18,
        lineHeight: 21,
        margin: 10
    },
    subHeading: {
        fontSize: 18,
        lineHeight: 21,
        margin: 10
    },
    inputsContainer: {
        marginVertical: 10,
        maxWidth: 300,
        width: '100%'
    },
    input: {
        marginHorizontal: 10,
        borderWidth: 1,
        borderColor: COLOR_BORDER,
        borderRadius: 10,
        height: 40,
        marginVertical: 4,
        justifyContent: 'center',
    },
    text: {
        paddingHorizontal: 10,
        fontSize: 14,
        color: COLOR_TEXT,
    },
    submitButton: {
        marginVertical: 40,
        alignItems: 'center',
        backgroundColor: COLOR_PRIMARY,
        flexDirection: 'row'
    },
    submitButtonText: {
        fontSize: 18,
        lineHeight: 21,
        color: COLOR_TEXT_PRIMARY_BUTTON
    },
    dobDoneTouchable: {
        alignSelf: 'flex-end',
        backgroundColor: COLOR_PRIMARY,
        margin: 5,
        marginTop: 0,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 10
    }
})