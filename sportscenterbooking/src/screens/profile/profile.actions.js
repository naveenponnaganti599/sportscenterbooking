import actionTypes from '../../redux/actions/action-types';

export const saveProfileDetails = (userDetails) => {
    let payload = {
        id: userDetails.id,
        firstName: userDetails.firstName,
        lastName: userDetails.lastName,
        email: userDetails.email,
        dateOfBirth: userDetails.dob,
        bloodGroup: userDetails.bloodGroup,
        tShirtSize: userDetails.tShirtSize,
    }
    return {
        type: actionTypes.UPDATE_USER_DETAILS,
        payload: payload
    }
}