import { connect } from 'react-redux';

import ProfileScreen from './profile.component';
import { saveProfileDetails } from './profile.actions';


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveProfileDetails: (userDetails) => { console.log("in connect"); dispatch(saveProfileDetails(userDetails)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);