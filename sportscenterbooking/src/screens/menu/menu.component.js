import React from 'react';
import { View, SafeAreaView, FlatList, StyleSheet, Image, TouchableOpacity, Text, StatusBar, Modal, ScrollView, Dimensions } from 'react-native';

import MenuCard from '../../components/menu-card.component';
import { COLOR_WHITE, COLOR_PRIMARY, COLOR_TEXT_PRIMARY_BUTTON } from '../../constants/colors';
import ImgFooter from '../../assets/images/img-try-out-a-sport.png';
import ImgBack from '../../assets/images/img-arrow-left.png';

const buttons = [
    { text: 'Home' },
    { text: 'Account Info' },
    { text: 'Sports Centers' },
    { text: 'Bookings' },
    { text: 'Sports Tournaments' },
    { text: 'Tournament Registrations' },
    { text: 'Runs' },
    { text: 'Running Registrations' },
    { text: 'Coaching' },
    { text: 'Membership Plans' },
    { text: 'Cancellation & Modification Rules' },
    { text: 'Partner with SWYNG' },
    { text: 'Support & Help / Contact' },
    { text: 'About SWYNG' },
    { text: 'Terms of use' },
    { text: 'Privacy Policy' },
    { text: 'Logout' },
]


const cities = [
    {
        id: 1,
        name: "Bengaluru"
    },
    {
        id: 2,
        name: "Chennai"
    },
    {
        id: 3,
        name: "Delhi NCR"
    },
    {
        id: 4,
        name: "Hyderabad"
    },
    {
        id: 5,
        name: "Pune"
    },
]

const { width, height } = Dimensions.get("window")


export default function Menu({ navigation, cities, preferredCity, updatePreferredCity }) {

    const MyHeader = function () {
        return (
            <View style={styles.headerContainer}>
                <SafeAreaView>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image style={styles.headerLeftImage} source={ImgBack} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.headerRightTouchable} onPress={() => { setShowCitySelection(true) }}>
                            <Text>{cities[preferredCity].name}</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView >
            </View>
        )
    }

    const [showCitySelection, setShowCitySelection] = React.useState(false);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: false,
        })
    })

    const getRenderItem = ({ item, index }) => {
        return <MenuCard text={item.text} key={index} />
    }


    return (
        <SafeAreaView style={styles.screen}>
            <FlatList
                bounces={false}
                style={styles.flatList}
                data={buttons}
                numColumns={2}
                renderItem={getRenderItem}
                keyExtractor={(_, index) => { return index.toString() }}
                ListHeaderComponent={
                    <MyHeader navigation={navigation} />
                }
                ListFooterComponent={() => {
                    return (
                        <Image style={styles.footerImage} source={ImgFooter} />
                    )
                }}
                ListFooterComponentStyle={styles.footer}
            />

            {
                showCitySelection &&
                <Modal presentationStyle="overFullScreen" transparent={true}>
                    <View style={styles.citySelectionOverlay}>
                        <View style={styles.citySelectionView}>
                            <SafeAreaView >
                                <View style={styles.safeAreaContainer}>
                                    <TouchableOpacity onPress={() => { setShowCitySelection(false) }}>
                                        <Image style={styles.headerLeftImage} source={ImgBack} />
                                    </TouchableOpacity>
                                </View>
                                <FlatList
                                    style={styles.citiesFlatList}
                                    data={Object.keys(cities)}
                                    numColumns={2}
                                    keyExtractor={(_, index) => index.toString()}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <View style={styles.cityView} key={index}>
                                                <TouchableOpacity onPress={() => { updatePreferredCity(item) }}
                                                    style={[styles.cityTouchable, item == preferredCity && styles.preferredCityTouchable]}>
                                                    <Text style={item == preferredCity && styles.preferredCityText}>{cities[item].name}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }}
                                />
                            </SafeAreaView>
                        </View>
                    </View>
                </Modal>
            }

        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: COLOR_WHITE
    },
    flatList: {
        flex: 1,
        paddingHorizontal: 6,
        paddingTop: 6
    },
    headerContainer: {
        backgroundColor: COLOR_WHITE
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 54
    },
    headerLeftImage: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        margin: 5,
        marginTop: 0
    },
    headerRightTouchable: {
        paddingVertical: 10,
        paddingHorizontal: 14,
        borderRadius: 10,
        backgroundColor: COLOR_WHITE,
        elevation: 6,
        marginRight: 10,
        shadowOpacity: 0.5,
        shadowRadius: 4,
        shadowOffset: { height: 4, width: 4 },
        shadowColor: 'rgba(0,0,0,0.5)'

    },
    footerImage: {
        resizeMode: 'contain',
        width: '80%',
        marginHorizontal: 20,
        alignSelf: 'center',
        maxHeight: 100
    },
    footer: {
        maxHeight: 100,
        justifyContent: 'center'
    },
    citySelectionOverlay: {
        ...StyleSheet.absoluteFillObject,
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.25)'
    },
    citySelectionView: {
        width: '100%',
        backgroundColor: COLOR_WHITE,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        paddingBottom: 20
    },
    safeAreaContainer: {
        paddingTop: 20,
        paddingLeft: 6
    },
    citiesFlatList: {
        paddingHorizontal: 6,
        marginTop: 10
    },
    cityView: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        flex: 1,
        flexDirection: 'row',
        marginVertical: 6,
        marginVertical: 8
    },
    cityTouchable: {
        shadowOffset: {
            height: 2,
            width: 4
        },
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowRadius: 4,
        shadowOpacity: 0.5,
        elevation: 3,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR_WHITE,
        padding: 10,
        flex: 1,
        maxWidth: 0.5 * width
    },
    preferredCityTouchable: {
        backgroundColor: COLOR_PRIMARY
    },
    preferredCityText: {
        color: COLOR_TEXT_PRIMARY_BUTTON
    }
})