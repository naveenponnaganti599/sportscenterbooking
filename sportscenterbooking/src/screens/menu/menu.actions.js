import actionTypes from '../../redux/actions/action-types';

export const updatePreferredCity = (id) => {
    return {
        type: actionTypes.UPDATE_PREFERRED_CITY,
        payload: id
    }
}