import { connect } from 'react-redux'
import MenuScreen from './menu.component';

import { updatePreferredCity } from './menu.actions'

const mapStateToProps = (state) => {
    return {
        preferredCity: state.user.preferredCity,
        cities: state.cities
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePreferredCity: (id) => { dispatch(updatePreferredCity(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);