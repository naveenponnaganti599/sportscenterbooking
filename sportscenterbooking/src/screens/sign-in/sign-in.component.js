import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    Keyboard,
    Image,
    ScrollView,
    Dimensions,
    StatusBar,
    Platform,
    SafeAreaView,
    ActivityIndicator
} from 'react-native';

import api from '../../services/api';
import imgNextPageActive from '../../assets/images/img-next-page-blue.png';
import imgNextPageInactive from '../../assets/images/img-next-page-grey.png';
import { SIGN_IN_TEXT, MOBILE_NUMBER_INPUT_PH, NEW_USER_TEXT, PHONE_NUMBER_DOES_NOT_EXIST } from './sign-in.constants';
import { COLOR_BORDER, COLOR_PRIMARY, COLOR_TEXT, COLOR_WHITE, COLOR_BLACK } from '../../constants/colors';
import { SIGN_UP_SCREEN, OTP_ENTRY_SCREEN, PROFILE_SCREEN } from '../../constants/navigation';
import { ANDROID } from '../../constants/strings';
import endpoints from '../../constants/endpoints';

const { height } = Dimensions.get('window');

const requestOTP = async (phoneNumber) => {
    let body = {
        phone: phoneNumber
    }
    return api.post(endpoints.POST_SEND_OTP, body);
}

export default function SignInScreen({ navigation, updatePhoneNumber }) {

    const [phoneInput, setPhoneInput] = React.useState("");
    const [isRequestingOTP, setIsRequestingOTP] = React.useState(false);

    React.useEffect(() => {
        Dimensions.addEventListener('change')
    }, [])

    const shouldActivateNextPage = () => {
        return phoneInput.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
    }

    const onNewUserPress = () => {
        navigation.navigate(SIGN_UP_SCREEN)
    }

    const onNextPagePress = () => {
        setIsRequestingOTP(true);
        requestOTP(phoneInput)
            .then(response => {
                setIsRequestingOTP(false);
                if (response.status == 200) {
                    updatePhoneNumber(phoneInput)
                    navigation.navigate(OTP_ENTRY_SCREEN);
                }
            })
            .catch(error => {
                setIsRequestingOTP(false);
                if (error.response && error.response.status == 400
                    && error.response.data && error.response.data.message == PHONE_NUMBER_DOES_NOT_EXIST) {
                    updatePhoneNumber(phoneInput)
                    navigation.navigate(PROFILE_SCREEN);
                }
            });
    }

    return (
        <ScrollView style={styles.scrollView} bounces={false}>
            <View style={styles.screen}>
                <View style={styles.topContainer}>
                    <View style={styles.signInTextContainer}>
                        <Text style={styles.signInText}>{SIGN_IN_TEXT}</Text>
                    </View>
                    <View style={styles.numberInputContainer}>
                        <TextInput
                            editable={!isRequestingOTP}
                            style={styles.numberInput}
                            placeholder={MOBILE_NUMBER_INPUT_PH}
                            keyboardType='number-pad'
                            maxLength={10}
                            value={phoneInput}
                            onChangeText={setPhoneInput}
                            onSubmitEditing={Keyboard.dismiss} />
                    </View>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.actionsContainer}>
                        <TouchableOpacity
                            disabled={isRequestingOTP}
                            style={styles.touchable}
                            onPress={shouldActivateNextPage() ? onNextPagePress : null}>
                            <Image
                                style={styles.nextPage}
                                source={shouldActivateNextPage() ?
                                    imgNextPageActive : imgNextPageInactive} />
                        </TouchableOpacity>
                    </View>
                    {
                        isRequestingOTP &&
                        <ActivityIndicator />
                    }
                    <View style={styles.newUserTextContainer}>
                        <TouchableOpacity onPress={onNewUserPress}>
                            <Text style={styles.newUserText}>{NEW_USER_TEXT}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    scrollView: {
        flex: 1,
        backgroundColor: COLOR_WHITE
    },
    screen: {
        flex: 1,
        minHeight: (
            Platform.OS == ANDROID
                ? height - StatusBar.currentHeight
                : height
        )
    },
    topContainer: {
        flex: 3
    },
    signInTextContainer: {
        flex: 4,
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        padding: 20
    },
    signInText: {
        textAlign: 'center',
        color: COLOR_TEXT,
        fontSize: 13,
        maxWidth: 350,
        lineHeight: 15
    },
    numberInputContainer: {
        flex: 3,
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        paddingTop: 10
    },
    numberInput: {
        width: '70%',
        height: 40,
        maxWidth: 250,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: COLOR_BORDER,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: COLOR_TEXT
    },
    bottomContainer: {
        flex: 4,
        alignItems: 'center'
    },
    actionsContainer: {
        width: '100%',
        maxWidth: 300,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end',
        padding: 15
    },
    touchable: {
    },
    previousPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    nextPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    newUserTextContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 15
    },
    newUserText: {
        color: COLOR_PRIMARY,
        textDecorationLine: 'underline'
    }
})