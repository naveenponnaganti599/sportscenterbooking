import { connect } from 'react-redux';

import SignInScreen from './sign-in.component';
import { updatePhoneNumber } from './sign-in.actions';

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePhoneNumber: (phoneNumber) => { dispatch(updatePhoneNumber(phoneNumber)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);