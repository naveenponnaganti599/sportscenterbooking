export const SIGN_IN_TEXT = "Sign in with your registered 10 digit mobile number. An OTP will be sent to the same.";
export const MOBILE_NUMBER_INPUT_PH = "Mobile No.";
export const NEW_USER_TEXT = "New User? Sign Up Here";
export const PHONE_NUMBER_DOES_NOT_EXIST = "User with phone number doesn't exist";
