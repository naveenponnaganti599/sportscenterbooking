import actionTypes from '../../redux/actions/action-types';

export const updatePhoneNumber = (phoneNumber) => {
    return {
        type: actionTypes.UPDATE_PHONE_NUMBER,
        payload: phoneNumber
    }
}