import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    Keyboard,
    Image,
    TouchableWithoutFeedback
} from 'react-native';

import imgNextPageActive from '../../assets/images/img-next-page-blue.png';
import imgNextPageInactive from '../../assets/images/img-next-page-grey.png';
import { COLOR_BORDER, COLOR_PRIMARY, COLOR_TEXT, COLOR_WHITE } from '../../constants/colors';
import { OTP_TEXT, EXISTING_USER_TEXT, MOBILE_NUMBER_INPUT_PH } from './sign-up.constants';
import { SIGN_IN_SCREEN, OTP_ENTRY_SCREEN } from '../../constants/navigation';

export default function SignUpScreen({ navigation }) {

    const onExistingUserPress = function () {
        navigation.navigate(SIGN_IN_SCREEN);
    }

    const shouldActivateNextPage = function () {

    }

    const [phoneInput, setPhoneInput] = React.useState();

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.screen}>
                <View style={styles.topContainer}>
                    <View style={styles.numberInputContainer}>
                        <TextInput
                            style={styles.numberInput}
                            placeholder={MOBILE_NUMBER_INPUT_PH}
                            keyboardType='number-pad'
                            value={phoneInput}
                            onChangeText={setPhoneInput}
                            onSubmitEditing={Keyboard.dismiss} />
                        <Text>{phoneInput}</Text>
                    </View>
                    <View style={styles.otpTextContainer}>
                        <Text style={styles.otpText}>{OTP_TEXT}</Text>
                    </View>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.actionsContainer}>
                        <TouchableOpacity
                            style={styles.touchable}
                            onPress={shouldActivateNextPage() ? onNextPagePress : null}>
                            <Image
                                style={styles.nextPage}
                                source={shouldActivateNextPage() ?
                                    imgNextPageActive : imgNextPageInactive} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.existingUserTextContainer}>
                        <TouchableOpacity onPress={onExistingUserPress}>
                            <Text style={styles.existingUserText}>{EXISTING_USER_TEXT}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: COLOR_WHITE
    },
    topContainer: {
        flex: 3
    },
    numberInputContainer: {
        flex: 4,
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        paddingTop: 10
    },
    numberInput: {
        width: '70%',
        height: 40,
        maxWidth: 250,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: COLOR_BORDER,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: COLOR_TEXT
    },
    otpTextContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        padding: 20
    },
    otpText: {
        textAlign: 'center',
        color: COLOR_TEXT,
        fontSize: 13,
        maxWidth: 350,
        lineHeight: 15
    },
    bottomContainer: {
        flex: 4,
        alignItems: 'center'
    },
    actionsContainer: {
        width: '100%',
        maxWidth: 300,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end',
        padding: 15
    },
    touchable: {
    },
    previousPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    nextPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    existingUserTextContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 15
    },
    existingUserText: {
        color: COLOR_PRIMARY,
        textDecorationLine: 'underline'
    }
})