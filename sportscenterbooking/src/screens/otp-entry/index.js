import { connect } from 'react-redux';

import OTPEntryScreen from './otp-entry-component';
import { updateToken } from './otp-entry.actions'

const mapStateToProps = (state) => {
    return {
        phoneNumber: state.user.phoneNumber
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateToken: (token) => { dispatch(updateToken(token)) }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(OTPEntryScreen);