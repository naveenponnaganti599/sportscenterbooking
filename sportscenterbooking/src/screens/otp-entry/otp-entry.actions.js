import actionTypes from '../../redux/actions/action-types';

export const updateToken = (token) => {
    return {
        type: actionTypes.UPDATE_AUTH_TOKEN,
        payload: token
    }
}