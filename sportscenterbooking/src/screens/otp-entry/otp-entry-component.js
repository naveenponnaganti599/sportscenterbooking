import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    Image,
    Keyboard,
    ScrollView,
    Dimensions,
    StatusBar,
    Platform,
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import api from '../../services/api';
import imgNextPageActive from '../../assets/images/img-next-page-blue.png';
import imgNextPageInactive from '../../assets/images/img-next-page-grey.png';
import imgPrevPageActive from '../../assets/images/img-prev-page-blue.png';
import { ENTER_OTP_TEXT } from './otp-entry.constants';
import { COLOR_WHITE, COLOR_TEXT, COLOR_BORDER, COLOR_PRIMARY } from '../../constants/colors';
import { DASHBOARD_SCREEN, PROFILE_SCREEN } from '../../constants/navigation';
import { ANDROID } from '../../constants/strings';
import OTPInput from '../../components/otp-input.component';
import endpoints from '../../constants/endpoints';
import { AUTH_TOKEN } from '../../constants/async-storage';

const { height } = Dimensions.get('window');

export default function OTPEntryScreen({ route, navigation, phoneNumber, updateToken }) {

    const { updateUserToken } = route.params;

    const [otp, setOTP] = React.useState();
    const [isValidatingOTP, setIsValidationOTP] = React.useState(false);



    const shouldActivateNextPage = () => {
        return otp > 99999 && otp <= 999999;
    }

    const onPrevPagePress = () => {
        navigation.goBack()
    }

    const validateOTP = () => {
        let body = {
            "phone": phoneNumber,
            "otp": otp.toString()
        }
        return api.post(endpoints.POST_VALIDATE_OTP, body)
    }

    const onNextPagePress = () => {
        validateOTP().then(response => {
            if (response.status == 200) {
                updateToken(response.data.token);
                AsyncStorage.setItem(AUTH_TOKEN, response.data.token)
            }
        }).catch(error => {

        })
    }

    return (
        <ScrollView style={styles.scrollView} bounces={false}>
            <View style={styles.screen}>
                <View style={styles.topContainer}>
                    <View style={styles.enterOTPTextContainer}>
                        <Text style={styles.enterOTPText}>{ENTER_OTP_TEXT}</Text>
                    </View>
                    <View style={styles.numberInputContainer}>
                        <OTPInput digitCount={6} setOTP={!isValidatingOTP && setOTP} />
                    </View>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.actionsContainer}>
                        <TouchableOpacity
                            disabled={isValidatingOTP}
                            style={styles.touchable}
                            onPress={onPrevPagePress}>
                            <Image
                                style={styles.nextPage}
                                source={imgPrevPageActive} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            disabled={isValidatingOTP}
                            style={styles.touchable}
                            onPress={shouldActivateNextPage() ? onNextPagePress : null}>
                            <Image
                                style={styles.nextPage}
                                source={shouldActivateNextPage() ?
                                    imgNextPageActive : imgNextPageInactive} />
                        </TouchableOpacity>
                    </View>
                    {
                        isValidatingOTP && <ActivityIndicator />
                    }
                </View>
            </View>
        </ScrollView>
    )

}

const styles = StyleSheet.create({
    scrollView: {
        flex: 1
    },
    screen: {
        flex: 1,
        backgroundColor: COLOR_WHITE,
        minHeight: (
            Platform.OS == ANDROID
                ? height - StatusBar.currentHeight
                : height
        )
    },
    topContainer: {
        flex: 3,
        justifyContent: 'center',
        width: '100%'
    },
    enterOTPTextContainer: {
        flex: 4,
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        padding: 20
    },
    enterOTPText: {
        textAlign: 'center',
        color: COLOR_TEXT,
        fontSize: 13,
        maxWidth: 350,
        lineHeight: 15
    },
    numberInputContainer: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
    },
    numberInput: {
        width: '70%',
        height: 40,
        maxWidth: 400,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: COLOR_BORDER,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: COLOR_TEXT
    },
    bottomContainer: {
        flex: 4,
        alignItems: 'center'
    },
    actionsContainer: {
        width: '100%',
        maxWidth: 300,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        padding: 15
    },
    touchable: {
    },
    previousPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    nextPage: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    newUserTextContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 15
    },
    newUserText: {
        color: COLOR_PRIMARY,
        textDecorationLine: 'underline'
    }
})