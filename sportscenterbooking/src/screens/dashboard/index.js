import { connect } from 'react-redux';
import DashboardScreen from './dashboard.component';

const mapStateToProps = (state) => {
    return {
        firstName: state.user.firstName
    }
}

export default connect(mapStateToProps)(DashboardScreen);