import React from 'react';
import { View, Text, FlatList, StyleSheet, Image, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';

import imgRectangle from '../../assets/images/img-rectangle.png';
import imgBadminton from '../../assets/images/img-badminton.png';
import { COLOR_BORDER, COLOR_WHITE } from '../../constants/colors';
import ImgHomeBlue from '../../assets/images/img-home-blue.png'
import Carousel from '../../components/carousel.component';
import UpcomingCard from '../../components/upcoming-card.component';
import TabBarIcon from '../../components/tab-bar-icon.component';
import ImgArrowRight from '../../assets/images/img-arrow-right.png';
import ImgFooter from '../../assets/images/img-try-out-a-sport.png';
import { MENU_SCREEN } from '../../constants/navigation';

const data = [
    {
        id: "1",
        sportName: "Badminton",
        eventName: "SWYNG Badminton Championship",
        time: "Sun 01 Nov 2019, Sarjapur Road",
        type: "Men’s Singles, Women’s Singles & More",
        image: imgBadminton
    },
    {
        id: "2",
        sportName: "Tennis",
        eventName: "SWYNG Tennis Championship",
        time: "Sun 01 Nov 2019, Sarjapur Road",
        type: "Men’s Singles, Women’s Singles & More",
        image: imgBadminton
    },
    {
        id: "3",
        sportName: "Badminton",
        eventName: "SWYNG Badminton Championship",
        time: "Sun 01 Nov 2019, Sarjapur Road",
        type: "Men’s Singles, Women’s Singles & More",
        image: imgBadminton
    },
    {
        id: "4",
        sportName: "Tennis",
        eventName: "SWYNG Tennis Championship",
        time: "Sun 01 Nov 2019, Sarjapur Road",
        type: "Men’s Singles, Women’s Singles & More",
        image: imgBadminton
    }
    // {
    //     id: "5",
    //     sportName: "Badminton",
    //     eventName: "SWYNG Badminton Championship",
    //     time: "Sun 01 Nov 2019, Sarjapur Road",
    //     type: "Men’s Singles, Women’s Singles & More",
    //     image: imgBadminton
    // },
    // {
    //     id: "6",
    //     sportName: "Tennis",
    //     eventName: "SWYNG Tennis Championship",
    //     time: "Sun 01 Nov 2019, Sarjapur Road",
    //     type: "Men’s Singles, Women’s Singles & More",
    //     image: imgRectangle
    // },
    // {
    //     id: "7",
    //     sportName: "Badminton",
    //     eventName: "SWYNG Badminton Championship",
    //     time: "Sun 01 Nov 2019, Sarjapur Road",
    //     type: "Men’s Singles, Women’s Singles & More",
    //     image: imgBadminton
    // },
    // {
    //     id: "8",
    //     sportName: "Tennis",
    //     eventName: "SWYNG Tennis Championship",
    //     time: "Sun 01 Nov 2019, Sarjapur Road",
    //     type: "Men’s Singles, Women’s Singles & More",
    //     image: imgRectangle
    // },
]

const upcomingRegistrations = [
    {
        name: "10K Run",
        time: "Thu 20 Jun 06.00 AM",
        type: "Midnight Marathon"
    },
    {
        name: "10K Run",
        time: "Thu 20 Jun 06.00 AM",
        type: "Midnight Marathon"
    }
]


export default function DashBoard({ navigation }) {

    const firstName = "Naval"

    const header = () => {
        return (
            <View style={styles.headerContainer} >
                <SafeAreaView >
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => navigation.navigate(MENU_SCREEN)} >
                            <Image style={styles.headerLeftIcon} source={ImgArrowRight} />
                        </TouchableOpacity>
                        <Text style={styles.headerText}>Hi {firstName}</Text>
                    </View>
                </SafeAreaView >
            </View >
        )
    }
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: true,
            header: header
        })
    })

    return (
        <SafeAreaView style={styles.screen}>
            <ScrollView style={styles.screen} bounces={false}>
                <View style={styles.carouselContainer}>
                    <Carousel data={data} />
                </View>
                <View style={styles.container}>
                    <UpcomingCard
                        data={upcomingRegistrations}
                        title={"Upcoming Registrations"}
                        leftTopKey={"name"}
                        rightTopKey={"time"}
                        rightBottomKey={"type"} />
                </View>
                <View style={styles.footer}>
                    <Image style={styles.footerImage} source={ImgFooter} />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        width: '100%',
        backgroundColor: 'white',
        paddingHorizontal: 4,
    },
    image: {
        width: '100%',
        resizeMode: 'stretch',
    },
    card: {
        marginBottom: 5,
        marginHorizontal: 10,
        elevation: 1,
        backgroundColor: 'white',
        borderRadius: 2,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    list: {
        flex: 1
    },
    cardTextContainer: {
        position: 'absolute',
        bottom: 1,
        padding: 15
    },
    sportName: {
        fontSize: 14,
        lineHeight: 16
    },
    eventName: {
        fontSize: 18,
        lineHeight: 21
    },
    time: {
        fontSize: 14,
        lineHeight: 16,
        fontWeight: '500'
    },
    type: {
        fontSize: 14,
        lineHeight: 16
    },
    carouselContainer: {
        paddingTop: 5
    },
    headerContainer: {
        backgroundColor: COLOR_WHITE,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.25)'
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 54
    },
    headerLeftIcon: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        margin: 12
    },
    headerText: {
        fontSize: 18,
        lineHeight: 21
    },
    footer: {
        maxHeight: 100,
        justifyContent: 'center',
        marginTop: 10
    },
    footerImage: {
        resizeMode: 'contain',
        width: '80%',
        marginHorizontal: 20,
        alignSelf: 'center',
        maxHeight: 100
    }
})
